/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

const { readdir } = require('fs').promises;

import { resolve } 
	from 'path';

import * as fs 
	from 'fs';

import * as path 
	from 'path';

import { Provider, ProjectInfoProvider }
	from "./Provider";

import { CompletionItem, CompletionItemKind, Connection, TextDocumentPositionParams, TextEdit, MarkupContent, SymbolKind, Command, Position, TokenFormat, TextDocumentSaveReason } 
	from "vscode-languageserver/node";

import Project, { NewLine, NewScope, ScopeType, Symbol, SymbolType } 
	from "../project/Project";

import LineUtils 
	from "../utils/LineUtils";

import { KickLanguage } 
	from "../definition/KickLanguage";

import StringUtils 
	from "../utils/StringUtils";

import PathUtils 
	from '../utils/PathUtils'; 

import { InstructionType } 
	from '../definition/KickInstructions';

import { URI } 
	from "vscode-uri";

import { ProjectFile } 
	from '../project/ProjectFile';

import { Settings } 
	from './SettingsProvider';

import 
	{ performance } from 'perf_hooks';

import { Parameter } 
	from "../definition/KickPreprocessors";

import { Method }
	from '../definition/KickInternalSymbols';

enum LanguageCompletionTypes {
	None,
	Instruction,
	Symbol,
	Label,
	Register,
	PreProcessor,
	Directive,
	Method,
	MultiLabel,
	Filename,
	Literal,
	Ignore
}

export default class CompletionProvider extends Provider {

	private settings: Settings;
	private project: Project;
	private textDocumentPosition: TextDocumentPositionParams;
	private multiLabels: {[key: string]: Symbol[]} = {};
	private symbols: Symbol[] = undefined;
	private projectFile: ProjectFile;

	constructor(connection: Connection, projectInfo: ProjectInfoProvider) {

		super(connection, projectInfo);

		connection.onCompletion((textDocumentPosition:TextDocumentPositionParams): undefined|Thenable<CompletionItem[]> => {

			// console.log("[CompletionProvider] onCompletion");
			let _startTime = performance.now();


			this.settings = this.getProjectInfo().getSettings();
			this.project = projectInfo.getProject(textDocumentPosition.textDocument.uri);
			this.textDocumentPosition = textDocumentPosition;
			this.symbols = this.project.getAllSymbols();

			let _items = this.createCompletionItems(projectInfo);

			let _endTime = performance.now();
			console.log(`onCompletion took ${_endTime - _startTime} milliseconds`);

			return _items;
		});

		connection.onCompletionResolve((item:CompletionItem):CompletionItem => {
			if (projectInfo.getSettings().valid) {
				return item;
			}
		});
    }

	private async createCompletionItems(projectInfo: ProjectInfoProvider): Promise<CompletionItem[]> {

		var _items: CompletionItem[] = [];
		var _lines: NewLine[] = [];
		var _source: string[] = [];
		var _triggerPosition: number;
		var _triggerLine: string;
		var _triggerScope: NewScope;
		var _trigger: string;
		var _triggerToken: string;
		var _tokensBefore: string[];
		var _tokensAfter: string[];
		var _cleanTriggerLine: string;
		var _tokens: string[];
		var _words: string[];
		var _wordsBefore: string[];

		// determines what kind of items to return to the developer
		let _completionType: LanguageCompletionTypes = LanguageCompletionTypes.None;

		let _project: Project = projectInfo.getProject(this.textDocumentPosition.textDocument.uri);
		let _projectFile: ProjectFile = _project.getProjectFile();
		this.projectFile = _projectFile;

		// handle special case of no project file available
		// todo: i think we can do better but will require fixing the project manager
		if (!_projectFile) {

			let _line = this.textDocumentPosition.position.line;
			let _source_lines = _project.getSourceLines();
			let _sl = _source_lines[_line];

			if (_sl.startsWith('#')) {
				_items = _items.concat(this.createProprocessorItems());
			}

			if (_sl.startsWith('.')) {
				_items = _items.concat(this.loadDirectives());
			}

			if (_sl.startsWith('*')) {
				_items = _items.concat(this.loadStar());
			}

			if (!_sl.startsWith('.') && !_sl.startsWith('#') && !_sl.startsWith('*')) {
				_items = _items.concat(this.createMacroItems(this.loadGlobalSymbolsOfType(SymbolType.Macro)));
				_items = _items.concat(this.createInstructionItems());
			}

			return _items;
	}


		// get line information
		_lines = _projectFile.getNewLines();
		_source = _projectFile.getSourceLines();

		// precalc some useful vars for item creation and scope checking

		try {

			_triggerPosition = this.textDocumentPosition.position.character - 1;
			_triggerLine = _source[this.textDocumentPosition.position.line];


			// if we have a line / file length mismatch then we should ignore
			// todo: this should probably be corrected before we get here

			if (this.textDocumentPosition.position.line != _lines.length) {
				_triggerScope = _lines[this.textDocumentPosition.position.line].scope;
			} else {
				_triggerScope = <NewScope>{};
				_triggerScope.name = "*"
				_triggerScope.level = 0;
				_triggerScope.type = ScopeType.Global;
					}

			
			_trigger = _triggerPosition >=0 ? _triggerLine[_triggerPosition] : "";
			_triggerToken = StringUtils.GetWordAt(_triggerLine, _triggerPosition);
			_tokensBefore = StringUtils.getWordsBefore(_triggerLine, _triggerPosition, false);
			_tokensAfter = StringUtils.GetWordsAfter(_triggerLine, _triggerPosition);
			_cleanTriggerLine = _triggerLine.replace(/\./g, " ").slice(0, _triggerPosition).trim();
			_tokens = StringUtils.splitIntoTokens(_cleanTriggerLine, true);
			_words = StringUtils.getWords(_triggerLine);
			_wordsBefore = StringUtils.getWordsBefore(_triggerLine, _triggerPosition, false);
		}
		catch (exception) {
			// console.log(exception);
			return this.createNullItem();
		}

		// when in remark block, return with no completion items
		if (LineUtils.isCursorInRemark(_source, this.textDocumentPosition)) {
			return this.createNullItem();
		}

		/*
			This part of the code checks to see when the cursor
			is inside a quote, and then determines if it is 
			part of a file selection request.
		*/
		
		let _in_string = _triggerLine.slice(0, _triggerPosition).replace(/("(?:[^"\\]|\\.)*"|'(?:[^'\\]|\\.)*')/g,"");
		let _in_string_match = _in_string.match(/["']/) || _triggerToken =='""' || _triggerToken =="''";

		if (_in_string_match) {

			if((_tokensBefore && 
				_tokensBefore[0].match(/\.*import(if)*/) ||
				_tokensBefore[_tokensBefore.length-1].slice(0,4) == "Load")
			) {
				var extensionFilter = '';
				if(_tokensBefore[0] === "#import" || (_tokensBefore[0] === ".import" && _tokensBefore[1] === "source") ){
					extensionFilter = this.settings.fileTypesSource;
				}
				if(_tokensBefore[_tokensBefore.length-1].slice(0,10) == "LoadBinary" || (_tokensBefore[0] === ".import" && _tokensBefore[1] === "binary") ){
					extensionFilter = this.settings.fileTypesBinary;
				}
				if(_tokensBefore[_tokensBefore.length-1].slice(0,7) == "LoadSid"){
					extensionFilter = this.settings.fileTypesSid;
				}
				if(_tokensBefore[_tokensBefore.length-1].slice(0,11) == "LoadPicture"){
					extensionFilter = this.settings.fileTypesPicture;
				}
				if(_tokensBefore[0] === ".import" && _tokensBefore[1] === "c64"){
					extensionFilter = this.settings.fileTypesC64;
				}
				if(_tokensBefore[0] === ".import" && _tokensBefore[1] === "text"){
					extensionFilter = this.settings.fileTypesText;
				}

				extensionFilter = extensionFilter.trim()	
					.replace(/  +/g,' ')	// reduce multiple spaces to one
					.replace(".","")		// remove possible extension dots
					.replace(/[, ]/g,"|")	//convert them to regex or
				;
				const sourcePath:string = PathUtils.getPathFromFilename(this.project.getUri());
				var foundFiles = await this.loadFilesystem(extensionFilter,PathUtils.uriToPlatformPath(sourcePath));

				var _libDirs;
				for(let i=0, il=this.settings.assemblerLibraryPaths.length; i < il; i++){
					let libPath = this.settings.assemblerLibraryPaths[i];
					if(!path.isAbsolute(libPath)) {
						libPath = path.join(sourcePath, libPath);
					}
					if (PathUtils.directoryExists(libPath)) {
						_libDirs = await this.loadFilesystem(extensionFilter,PathUtils.uriToPlatformPath(URI.file(libPath).toString()));
						if(_libDirs.length>0) {
							foundFiles.push(..._libDirs);
						}
					}
				}
				if(foundFiles.length === 0){
					// to prevent confusing word proposals (it's a central setting in vscode) return at least one item 
					foundFiles.push(<CompletionItem> {
						label: "No matching files found",
						kind: CompletionItemKind.File,
						data: {
							payload:{}
						}
					});
				}
				return foundFiles;
			}
			return this.createNullItem();
		}

		/*
			This part of the code checks to see what type of token has been
			entered (if any) before loading the list of completion
			items. This helps to provide some context to what is provided
			to the developer. For example, if an instruction was just entered
			like LDA, it make no sense to show the list of instruction
			codes again to the developer.
		*/

		// check for named label
		const _rwords = Array.from(_wordsBefore);
		_rwords.reverse();
		if (_rwords.length > 0 && _rwords[0].includes(':')) {
			_completionType = LanguageCompletionTypes.Label;
		}

		// loop thru tokens in reverse order to identify current token
		if (_completionType == LanguageCompletionTypes.None) {
	
			let _rtokens = Array.from(_tokens);
			_rtokens.reverse();

			for (let rtoken of _rtokens) {
			// const rtoken = _rtokens[0];
			// if (rtoken) {

				// check for instruction
				if (_completionType == LanguageCompletionTypes.None) {

					let _matchInstruction = KickLanguage.Instructions.find((token) => {
						return token.name.toLowerCase() === rtoken.toLowerCase();
					});
		
					if (_matchInstruction) {
						_completionType = LanguageCompletionTypes.Instruction;
					}
				}

				// check for directive
				if (_completionType == LanguageCompletionTypes.None) {

					let _symbols = this.project.getDirectives();
					let _matchSymbol = _symbols.find((token) => {
						return token.name.toLowerCase().slice(1) === rtoken;
					});
		
					if (_matchSymbol) {
						_completionType = LanguageCompletionTypes.Directive;
					}
				}
				
				// check for pre processor
				if (_completionType == LanguageCompletionTypes.None) {

					let _symbols = this.project.getPreprocessorSymbols();
					let _matchSymbol = _symbols.find((token) => {
						return token.name.toLowerCase().slice(1) === rtoken;
					});
		
					if (_matchSymbol) {
						_completionType = LanguageCompletionTypes.PreProcessor;
					}
				}
				
				// check for symbols
				if (_completionType == LanguageCompletionTypes.None) {

					let _symbols = this.project.getAllSymbols();
					let _matchSymbol = _symbols.find((token) => {
						return token.name === rtoken;
					});
		
					if (_matchSymbol) {
						_completionType = LanguageCompletionTypes.Symbol;
					}
				}
			}
		}

		// when second word after instruction starting with ! show multi labels
		if ((_completionType == LanguageCompletionTypes.Instruction || _completionType == LanguageCompletionTypes.Symbol) && _triggerToken[0][0] == '!') {
			return this.loadMutilabels(_triggerScope);
		}

		// check if we are potentially inside a parenthesis ()
		let _inparen: boolean = false;
		let _pos = _triggerLine.lastIndexOf("(");
		if (_pos > 0 && _trigger != ".") {
			if (_pos <= _triggerPosition) _inparen = true;
		}
		// are we outside of the parenthesis
		_pos = _triggerLine.lastIndexOf(")");
		if (_pos <= _triggerPosition) {
			_inparen = false;
		}

		// check to see if we want to show children or methods when triggered
		if (_completionType == LanguageCompletionTypes.Symbol) {


			let _clean = _triggerToken.replace("#", "");
			let _split = _clean.split(/(?<=\.)/);
			let _sw = [];

			for (let word of _split) {
				if (word.includes(".")) {
					_sw.push(word.replace(".", ""));
				}
			}

			let _swr = _sw.reverse();

			if (_swr.length > 0 && !_inparen) {

				let _name = _swr[0];
				let _parent = _swr[1];
	
				// check for variables at current scope
				let _symbols = this.getScopedSymbolsOfType(SymbolType.Variable, _triggerScope);

				let _symbol = _symbols.find((symbol) => {
					if (symbol.type == SymbolType.Variable && symbol.name === _name) {
						return symbol; 
					}
				});
	
				if (_symbol) {
	
					// look for matching function on original value
					let _pos = _symbol.originalValue.indexOf("(");
					if (_pos > 0) {
	
						// check for current word match on directive
						var _bis = this.project.getBuiltInSymbols();
						var _sn = _symbol.originalValue.slice(0, _pos).toLowerCase();
	
						// search for matching symbol name
						let _s: Symbol = _bis.find((symbol: Symbol) => {
							if(symbol.name.toLowerCase() === _sn) {
								return symbol;
							}
						});
	
						if (_s &&  _s.methods) {
							for (let method of _s.methods) {
								_items.push(this.createMethodItem(method));
							}
							return _items;
						}
	
					}
				} else {
	
					if(_sw.length > 0) {
						let _items = this.loadChildren(_name, _parent, _triggerScope);
						return _items;
					}
				}
			}
		}
		
		// when entering literals, return nothing
		if (_triggerToken.length > 0 && (_triggerToken.startsWith('#$') || _triggerToken.startsWith('#%') || _triggerToken.startsWith('$') || _triggerToken.startsWith('#>$') || _triggerToken.startsWith('#<$'))) {
			return this.createNullItem();
		}

		// when trigger is comma and not entering parms, return nothing
		if (_trigger.startsWith(",") && !_inparen) {
			return this.createNullItem();
		}

		// check for special case for when certain trigger tokens are used like #, !, . and so on
		if (_triggerToken.length > 0) {

			// when prev token starting with # show pre-processor items
			if (_completionType == LanguageCompletionTypes.None && _triggerToken[0][0] == '#') {
					_items = _items.concat(this.createProprocessorItems());
				return _items;
			}

			// when prev token starting with . show directive items
			if ((_completionType == LanguageCompletionTypes.None || _completionType == LanguageCompletionTypes.Label) && _triggerToken[0][0] == '.') {
					_items = _items.concat(this.loadDirectives());
				return _items;
			}

			// when prev token starting with ! show nothing
			if (_completionType == LanguageCompletionTypes.None && _triggerToken[0][0] == '!') {
				return this.createNullItem();
			}

			// when second word starting with # do nothing
			if (_completionType == LanguageCompletionTypes.Instruction && _triggerToken[0][0] == '#') {
			}

			// when second word starting with . do nothing
			if (_completionType == LanguageCompletionTypes.Instruction && _triggerToken[0][0] == '.') {
			}

		}

		// when there is no first token
		if (_completionType == LanguageCompletionTypes.None) {
			_items = _items.concat(this.createInstructionItems());
			_items = _items.concat(this.createScopedMacroItems(_triggerScope));
			_items = _items.concat(this.createScopedPseudoItems(_triggerScope));
			_items = _items.concat(this.loadStar());
			_items = _items.concat(this.createMacroItems(this.loadGlobalSymbolsOfType(SymbolType.Macro)));
			_items = _items.concat(this.createPseudoItems(this.loadGlobalSymbolsOfType(SymbolType.PseudoCommand)));
		}

		// when the first token is an instruction, directive or symbol 
		if (_completionType == LanguageCompletionTypes.Instruction || _completionType == LanguageCompletionTypes.Directive || _completionType == LanguageCompletionTypes.Symbol) {
			// console.log("[CompletionProvider] instruction");
			_items = _items.concat(this.loadSymbols(SymbolType.Namespace, _triggerScope));
			_items = _items.concat(this.loadSymbols(SymbolType.Variable, _triggerScope));
			_items = _items.concat(this.loadSymbols(SymbolType.NamedLabel, _triggerScope));
			_items = _items.concat(this.loadSymbols(SymbolType.Label, _triggerScope));
			_items = _items.concat(this.loadSymbols(SymbolType.Parameter, _triggerScope, '700'));
			_items = _items.concat(this.loadSymbols(SymbolType.Constant, _triggerScope, '800'));
			_items = _items.concat(this.createScopedFunctionItems(_triggerScope));
			_items = _items.concat(this.createFunctionItems(this.loadGlobalSymbolsOfType(SymbolType.Function)));
		}

		// when first word ends with : (label) show instructions, macros & pseudo commands
		if (_completionType == LanguageCompletionTypes.Label) {
			_items = _items.concat(this.createInstructionItems());
			_items = _items.concat(this.createScopedMacroItems(_triggerScope));
			_items = _items.concat(this.createScopedPseudoItems(_triggerScope));
			_items = _items.concat(this.createMacroItems(this.loadGlobalSymbolsOfType(SymbolType.Macro)));
			_items = _items.concat(this.createPseudoItems(this.loadGlobalSymbolsOfType(SymbolType.PseudoCommand)));
		}

		if (_items.length == 0) {
			return this.createNullItem();		
		}

		return _items;
	}

	private createNullItem(): CompletionItem[] {
		var _items: CompletionItem[] = [];
		_items.push(this.createCompletionItem("", LanguageCompletionTypes.Symbol, CompletionItemKind.Constant, null));
		return _items;
	}

	private createInstructionItems(): CompletionItem[] {

		var _items: CompletionItem[] = [];

		for (let item of KickLanguage.Instructions) {

			const _name = item.name.toLocaleLowerCase();

			if (!item.type ||
				(item.type === InstructionType.Illegal && this.settings.opcodes.illegal) ||
				(item.type === InstructionType.DTV && this.settings.opcodes.DTV) ||
				(item.type === InstructionType.C02 && this.settings.opcodes["65c02"]) ||
				(item.type === InstructionType.CE02 && this.settings.opcodes["65ce02"]) ||
				(item.type === InstructionType.GS02 && this.settings.opcodes["45gs02"])
			) {

				let _item:CompletionItem = this.createCompletionItem(_name, LanguageCompletionTypes.Instruction, CompletionItemKind.Interface, null);

				// add tab after completion
				if (this.settings.editor.tabAfterInstruction) {
					let _command: Command = <Command>{};
					_command.command = 'tab';
					_item.command = _command;
				}

				_item.documentation = this.createDocumentation(item);
				_item.sortText = '100';
				_items.push(_item);
			}
		}

		return _items;
	}

	private createProprocessorItems(): CompletionItem[] {

		var _items: CompletionItem[] = [];

		for (let item of KickLanguage.PreProcessors) {

			let _name: string = item.name.slice(1);

			let _item:CompletionItem = this.createCompletionItem(item.name, LanguageCompletionTypes.PreProcessor, CompletionItemKind.Property, null);
			_item.filterText = _name;

			// add snippet when present
			if (item.snippet) {
				_item = this.addItemSnippet(_item, _name, item.snippet );
			}

			_items.push(_item);
		}

		return _items;
	}

	/**
	 * Return available Macros and create proper
	 * snippet format for parameters.
	 * 
	 * @param textDocumentPosition 
	 * @returns array of Macro items
	 */
	private createScopedMacroItems(scope: NewScope): CompletionItem[] {
		return this.createMacroItems(this.getScopedSymbolsOfType(SymbolType.Macro, scope));
	}

	/**
	 * From a list of Macro Symbols return a list of Macro Completion Items
	 * 
	 * @param symbols 
	 * @returns 
	 */
	private createMacroItems(symbols: Symbol[]): CompletionItem[] {

		var _items: CompletionItem[] = [];

		for (let symbol of symbols) {
			_items.push(this.createMacroItem(symbol));
		}

		return _items;
	}

	/**
	 * From a Macro Symbol return a Macro Completion Item
	 * @param symbol \
	 * @returns 
	 */
	private createMacroItem(symbol:Symbol):CompletionItem {

		let _item:CompletionItem = this.createCompletionItem(symbol.name, LanguageCompletionTypes.Symbol, CompletionItemKind.Method, null);

		_item.sortText = '100'

		// build parms when placeholders requested
		let _snippetParms: string = '$1';
		let _snippetParm: number = 0;

		if (this.settings.completionParameterPlaceholders && symbol.parameters) {

			_snippetParms = ''; // clear the parms
			for (let parm of symbol.parameters) {
				_snippetParm += 1;
				_snippetParms += `\${${_snippetParm}:${parm.name}}, `;
			}

			// remove last comma (lazy)
			_snippetParms = _snippetParms.slice(0, _snippetParms.length - 2);
		}

		let _snippet = `${symbol.name}(${_snippetParms})\n`;

		_item.insertText = _snippet;
		_item.insertTextFormat = 2; // snippet

		// trigger parameter hints always
		let _command: Command = <Command>{};
		_command.command = 'editor.action.triggerParameterHints';
		_item.command = _command;

		// use built in snippet when available
		if (symbol.isBuiltin && symbol.snippet) {
			_item.insertText = `${symbol.name}${symbol.snippet}`;
		}

		// add documentation
		_item.documentation = this.createDocumentation(symbol);

		return _item;
	}

	private createScopedFunctionItems(scope: NewScope): CompletionItem[] {
		return this.createFunctionItems(this.getScopedSymbolsOfType(SymbolType.Function, scope));
	}

	private createFunctionItems(symbols: Symbol[]): CompletionItem[] {

		var _items: CompletionItem[] = [];

		for (let symbol of symbols) {
			_items.push(this.createFunctionItem(symbol));
		}

		return _items;
	}

	/**
	 * Return available Functions and create proper
	 * snippet format for parameters.
	 * 
	 * @param textDocumentPosition 
	 * @returns array of Macro items
	 */
	private createFunctionItem(symbol: Symbol): CompletionItem {

		let _item:CompletionItem = this.createCompletionItem(symbol.name, LanguageCompletionTypes.Symbol, CompletionItemKind.Function, null);

		_item.sortText = '998';

		// build parms when placeholders requested
		let _snippetParms: string = '$1';
		let _snippetParm: number = 0;

		if (this.settings.completionParameterPlaceholders && symbol.parameters) {

			_snippetParms = ''; // clear the parms
			for (let parm of symbol.parameters) {

				_snippetParm += 1;
				var _parm = `\${${_snippetParm}:${parm.name}}`;

				// add quotes for filename types
				if (parm.kind == SymbolKind.File) {
					_parm = `"\${${_snippetParm}}"`;
				}

				_snippetParms += `${_parm}, `;
			}

			// remove last comma (lazy)
			_snippetParms = _snippetParms.slice(0, _snippetParms.length - 2);
		}

		let _snippet = `${symbol.name}(${_snippetParms})$0`;

		_item.insertText = _snippet.trim();
		_item.insertTextFormat = 2; // snippet

		// trigger parameter hints always
		let _command: Command = <Command>{};
		_command.command = 'editor.action.triggerSuggest';
		_item.command = _command;
		_item.documentation = this.createDocumentation(symbol);

		return _item;
	}

	private createScopedPseudoItems(scope: NewScope): CompletionItem[] {
		return this.createPseudoItems(this.getScopedSymbolsOfType(SymbolType.PseudoCommand, scope));
	}

	private createPseudoItems(symbols: Symbol[]): CompletionItem[] {

		var _items: CompletionItem[] = [];

		for (let symbol of symbols) {
			_items.push(this.createPseudoItem(symbol));
		}

		return _items;
	}

	/**
	 * Return available Pseudo Commands and create proper
	 * snippet format for parameters.
	 * 
	 * @param textDocumentPosition 
	 * @returns array of Pseudo Command items
	 */
	private createPseudoItem(symbol: Symbol): CompletionItem {

		let _item:CompletionItem = this.createCompletionItem(symbol.name, LanguageCompletionTypes.Symbol, CompletionItemKind.Method, null);

		// build parms when placeholders requested
		let _snippetParms: string = '';
		let _snippetParm: number = 0;

		if (this.settings.completionParameterPlaceholders && symbol.parameters) {

			for (let parm of symbol.parameters) {
				_snippetParm += 1;
				_snippetParms += `\${${_snippetParm}:${parm.name}} : `;
			}

			// remove last colon (lazy)
			_snippetParms = _snippetParms.slice(0, _snippetParms.length - 2);
		}

		let _snippet = `${symbol.name} ${_snippetParms}$0`;

		_item.insertText = _snippet.trim();
		_item.insertTextFormat = 2; // snippet

		// trigger parameter hints always
		let _command: Command = <Command>{};
		_command.command = 'editor.action.triggerParameterHints';
		_item.command = _command;
		_item.documentation = this.createDocumentation(symbol)
		return _item;
	}

	private loadSymbols(type: SymbolType, scope: NewScope = undefined, sort: string = '000'): CompletionItem[] {

		// console.log(scope);

		var _items: CompletionItem[] =[];

		let _name: string = "*";
		let _parent: string = "*";

		if (scope) {
			_name = scope.name;
			_parent = scope.parent.name;
		}

		let _symbols = this.getScopedSymbolsOfType(type, scope);
		for (let symbol of _symbols) {

			if (symbol.type == type) {

				let _item:CompletionItem = this.createCompletionItem(symbol.name, LanguageCompletionTypes.Symbol, symbol.completionKind, null);

				_item.documentation = this.createDocumentation(symbol);

				// add weight for non built-in
				if (symbol.isBuiltin) {
					_item.sortText = sort;
				}

				// add  symbol
				_item.kind = symbol.completionKind;

				_items.push(_item);
			}
		}

		// const _ids = _items.map(o => o.label);
		// const _filtered = _items.filter(({label}, index) => !_ids.includes(label, index + 1))

		// return _filtered;

		let _unique = [];
		let _uniqueNames = [];

		_items.forEach((item) => {
			if (!_uniqueNames.includes(item.label)) {
				_uniqueNames.push(item.label);
				_unique.push(item);
			}
		});

		return _unique;
	}

	private loadDirectives(): CompletionItem[] {
		
		var _items: CompletionItem[] = [];

		for (let item of KickLanguage.Directives) {

				let _item:CompletionItem = this.createCompletionItem(item.name, LanguageCompletionTypes.Symbol, CompletionItemKind.Field, null);

				// add snippet when present
				if (item.snippet) {
					_item = this.addItemSnippet(_item, item.name.slice(1), item.snippet );
				}

				_item.documentation = this.createDocumentation(item);				
				_items.push(_item);
		}

		return _items;
	}

	/**
	 * Special Case of returning the '*' completion item
	 * to let the developer define the memory position
	 * for the next instruction.
	 * 
	 */
	 private loadStar(): CompletionItem[] {

		var _items: CompletionItem[] = [];

		let _item:CompletionItem = this.createCompletionItem(KickLanguage.Star.name, LanguageCompletionTypes.Label, CompletionItemKind.Value, null);

		// build snippet parms
		let _snippetParms: string = KickLanguage.Star.snippet;

		let _snippet = `${_snippetParms}$0`;

		_item.insertText = _snippet;
		_item.insertTextFormat = 2; // snippet

		// trigger parameter hints always
		let _command: Command = <Command>{};
		_command.command = 'editor.action.triggerParameterHints';
		_item.command = _command;
		_item.documentation = this.createDocumentation(KickLanguage.Star);
		_items.push(_item);
		return _items;
	}

	/**
	 * Returns an Array of Multi-Label in the
	 * current source file.
	 * 
	 * @returns An Array of Multi-Label Completion Items.
	 */
	private loadMutilabels(scope: NewScope): CompletionItem[] {

		var _items: CompletionItem[] = [];

		this.multiLabels = {};
		
		// let _symbols = this.project.getAllSymbols();
		const _symbols = this.getScopedSymbolsOfType(SymbolType.NamedLabel, scope, true, true);
		for (let item of _symbols) {

			// todo: fix to only show ! from the open file, not MAIN
			if (item.name[0] == "!" && item.isMain && item.type == SymbolType.NamedLabel) {

				let _item:CompletionItem = this.createCompletionItem(item.name, LanguageCompletionTypes.Symbol, CompletionItemKind.Reference, null);

				// add +/- scope to label
				let multiLabelsKey = item.name + String(item.scope);
				if(!this.multiLabels[multiLabelsKey]) {
					this.multiLabels[multiLabelsKey] = this.project.getSymbols().filter(symbol => {
						return symbol.name == item.name && symbol.scope === item.scope && symbol.type == SymbolType.NamedLabel;
					});
				}
				let multiLabelAddon = "";
				let howManyMinusSymbols = this.multiLabels[multiLabelsKey].filter(symbol => {
					return symbol.range.start.line <= this.textDocumentPosition.position.line;
				}).length;

				for(var p=0,pL=this.multiLabels[multiLabelsKey].length;p<pL;p++){
									
					if (this.multiLabels[multiLabelsKey][p].range.start.line > this.textDocumentPosition.position.line) {
						if(multiLabelAddon.indexOf("-") >= 0) {
							multiLabelAddon="";
						}
						multiLabelAddon += "+";
					} else {
						multiLabelAddon += "-";
					}

					if(this.multiLabels[multiLabelsKey][p].range.start.line === item.range.start.line) {
						break;
					}
				}
				if(multiLabelAddon[0] == "-") {
					multiLabelAddon = "-".repeat((howManyMinusSymbols-multiLabelAddon.length)+1);	
				}
				_item.label += multiLabelAddon;
				_item.filterText = _item.label.slice(1);
				_item.insertText = _item.label.slice(1);
				// _item.insertTextFormat = 2; // snippet

				_item.documentation = this.createDocumentation(item);				
				_items.push(_item);

			}
		}

		return _items;
	}

	private loadGlobalSymbolsOfType(type: SymbolType): Symbol[] {

		let _symbols: Symbol[] = [];

		_symbols = this.symbols.filter((symbol) => {
			if ((symbol.isBuiltin || symbol.isGlobal) && symbol.type == type) {
				return symbol;
			}
		});

		return _symbols;
	}

	// get symbols of certain types at specific scope
	private getScopedSymbolsOfType(type: SymbolType, scope: NewScope = undefined, recursive: boolean = true, includeMultilabel: boolean = false): Symbol[] {

		var _scopedSymbols: Symbol[] = [];

		const _typedSymbols: Symbol[] = this.getSymbolsOfType(type);
		let _symbols = this.findScopedSymbols(scope, _typedSymbols, recursive);

		for (let symbol of _symbols) {

			// skip multilabels
			if (symbol.name.startsWith('!')) {
				if (!includeMultilabel) {
					continue;
				}
			}

			// skip globals
			if (symbol.isGlobal) {
				if (!includeMultilabel) {
					continue;
				}
			}

			_scopedSymbols.push(symbol);
		}

		return _scopedSymbols;
	}

	private getSymbolsOfType(type: SymbolType): Symbol[] {

		return this.symbols.filter((symbol) => {
			if (symbol.type == type)
				return symbol;
		});
	}

	private loadChildren(name: string, parent: string, scope: NewScope): CompletionItem[] | undefined{

		var _items: CompletionItem[] = [];																																																																																																																																																																																																																																																																																																																																																																																																														[];
		var _symbols: Symbol[] = [];

		// _symbols = _symbols.concat(this.getScopedSymbolsOfType(SymbolType.NamedLabel, scope));
		// _symbols = _symbols.concat(this.getScopedSymbolsOfType(SymbolType.Label, scope));
		// _symbols = _symbols.concat(this.getScopedSymbolsOfType(SymbolType.Namespace, scope));

		_symbols = _symbols.concat(this.getSymbolsOfType(SymbolType.NamedLabel));
		_symbols = _symbols.concat(this.getSymbolsOfType(SymbolType.Label));
		_symbols = _symbols.concat(this.getSymbolsOfType(SymbolType.Namespace));

		// get all symbols that match the name
		let _matchingSymbols: Symbol[] = _symbols.filter((symbol) => {
				if (symbol.name === name) {
				return symbol; 
			}
		});

		// when nothing is found, just leave, perhaps not the most elegant solution?
		var _symbol: Symbol = _matchingSymbols[0];
		if (!_symbol) return;

		// create a new scope for the symbols we just found
		let _scope = <NewScope>{};
		_scope.name = _symbol.name;
		_scope.level = 0;
		_scope.parent = <NewScope>{};
		_scope.parent.name = _symbol.newScope.name;

		// second search for symbols in the current scope only
		_symbols = [];
		_symbols = _symbols.concat(this.getScopedSymbolsOfType(SymbolType.NamedLabel, _scope, false));
		_symbols = _symbols.concat(this.getScopedSymbolsOfType(SymbolType.Label, _scope, false));
		_symbols = _symbols.concat(this.getScopedSymbolsOfType(SymbolType.Namespace, _scope, false));

		for (let symbol of _symbols) {

			// do not include built-ins
			if (symbol.isBuiltin) continue;

			let _item:CompletionItem = this.createCompletionItem(symbol.name, LanguageCompletionTypes.Symbol, symbol.completionKind, null);

			// we will do better job of creating soon, for now the name and remarks is good enuf
			_item.documentation = this.createDocumentation(symbol);
			_items.push(_item);

		}

		// filter out any duplicates, again, probably not the best solution at the moment
		const _ids = _items.map(o => o.label);
		const _filtered = _items.filter(({label}, index) => !_ids.includes(label, index + 1))
		return _filtered;
	}

	private createCompletionItem(name: string, completionType: LanguageCompletionTypes, completionKind: CompletionItemKind, payload: any) {

		var _item: CompletionItem = <CompletionItem>{};

		_item.label = name;
		_item.data = {};
		_item.sortText = "000";
		_item.kind = completionKind;

		return _item;
	}

	private addItemSnippet(item: CompletionItem, label: string, snippet: string, paramaters?: Parameter[], separator: string = ' '): CompletionItem | undefined{

		var _snippet: string = "";

		// when there is no snippet, just return
		if (!snippet) return item;

		// add space when requested

		item.insertText = `${label}${separator}${snippet}`;
		item.insertTextFormat = 2; // snippet

		// force trigger suggest when asked, but not when only a line break
		const _len = snippet.length;
		const _match = snippet[0] == '\n';
		if (_len > 1 && !_match && snippet != '$0') {
			let _command: Command = <Command>{};
			_command.command = 'editor.action.triggerSuggest';
			item.command = _command;
		}

		return item;
	}

	private createDocumentation(symbol: any): string | MarkupContent | undefined {

		let _documentation: string | MarkupContent = symbol.description || symbol.comments || symbol.codeSneakPeek ? {
			value:	((symbol.description || symbol.comments || '') +
					(symbol.example ? "\n***\n"+symbol.example : "") +
					(symbol.deprecated ? "\n***\n*(deprecated)*" : "") + 
					(symbol.codeSneakPeek ? "\n***\nSneak Peek\n```\n"+symbol.codeSneakPeek+"\n```\n" : "") +
					(symbol.type == LanguageCompletionTypes.Instruction && symbol.type ? (
						(symbol.type == InstructionType.Illegal ? "\n***\n**(Illegal opcode)**" : "") + 
						(symbol.type == InstructionType.DTV ? "\n***\n**(DTV opcode)**" : "") + 
						(symbol.type == InstructionType.C02 ? "\n***\n**(65c02 opcode)**" : "") +
						(symbol.type == InstructionType.CE02 ? "\n***\n**(65ce02 opcode)**" : "") +
						(symbol.type == InstructionType.GS02 ? "\n***\n**(45gs02 opcode)**" : "")
					):"")).replace(/^\n\*\*\*\n/,""),
			kind: 'markdown'
		} : "";

		return _documentation;
	}

	private createMethodItem(method: Method): CompletionItem {
		var _item = this.createCompletionItem(method.name, LanguageCompletionTypes.Symbol, CompletionItemKind.Method, null);
		_item = this.addItemSnippet(_item, method.name, method.snippet, method.parameters, '');
		return _item;
	}

	private async loadFilesystem(extensionFilter:string, dir:string,base?:string):Promise<CompletionItem[]> {

		if (!base) base = dir;
		// make setting entries dynamic :)
		let outputDirectory = this.getProjectInfo().getSettings().assembler.option.outputDirectory;
		//Remove this when removing deprecated settings
		if(outputDirectory === ''){
            outputDirectory = this.getProjectInfo().getSettings().outputDirectory;
        }
		const currentEditorFile = path.basename(this.project.getUri());
		const dirents = await readdir(dir, { withFileTypes: true });
		const files = await Promise.all(
			dirents
			.filter((dirent: fs.Dirent) => {
				const ext = path.extname(dirent.name);
				const extReg = new RegExp("\\.("+extensionFilter+")", "i");
				return dirent.name !== currentEditorFile && dirent.name !== outputDirectory && dirent.name[0] !== '.' && (extensionFilter == "" || dirent.isDirectory() || ext.match(extReg));
			})
			.map((dirent: fs.Dirent) => {
				const res = resolve(dir, dirent.name);
				const ext = path.extname(dirent.name);
				const relPath = res.replace(base,"").slice(1);
				const documentation = ext.match(/\.(jpg|png|gif)/i) ? {
					value: `![](file://${res})\n\n\n\n\n\n\n\n\#### Preview`,
					kind: 'markdown'
				} : '';
				return dirent.isDirectory() ? this.loadFilesystem(extensionFilter,res,base) : <CompletionItem> {
					label: relPath,
					kind: CompletionItemKind.File,
					documentation,
					data: {
						payload:{}
					}
				};
			})
		);
		var cleanedFiles = Array.prototype.concat(...files).filter((entry:CompletionItem) => {
			return !!entry.label;
		});
		return cleanedFiles;
	}

	private findScopedSymbols(scope: NewScope, symbols: Symbol[], recursive: boolean = true): Symbol[] {

		let _scope = scope;
		let _symbols: Symbol[] = [];

		while (true) {

			for (let symbol of symbols) {
				if (symbol.newScope && symbol.newScope.name == _scope.name && symbol.newScope.level == _scope.level) {
					_symbols.push(symbol);
				}
			}

			if (_scope.name == '*')
				break;

			if (!recursive)
				break;

			_scope = _scope.parent;

		}
 	return _symbols;
	}

}
