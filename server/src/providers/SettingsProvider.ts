/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { 
    Provider, ProjectInfoProvider 
} from "./Provider";

import { 
    Connection,
    DidChangeConfigurationParams
 } from "vscode-languageserver/node";
import Project from "../project/Project";
import { Assembler, AssemblerResults } from "../assembler/Assembler"
import * as path from 'path';
import PathUtils from "../utils/PathUtils";
import * as fs from "fs";
import * as opn from "open";
import { URI } from "vscode-uri";
import * as os from 'os';
import { spawnSync } from "child_process";

/*

*/

export interface GlobalSettings {
    ['kickassembler']:Settings;
}

export interface SettingsOpcodes {
    "65c02":boolean;
    "65ce02":boolean;
    "45gs02":boolean;
    DTV: boolean;
    illegal: boolean;
}

export interface JavaPluginSettings {
    list: string[],
    properties: string[],
}

export interface JavaSettings {
    runtime: string,
    options: string,
    plugin: JavaPluginSettings
}

export interface AssemblerOptions {
    afo: boolean,
    binfile: boolean,
    cfgfile: string,
    viceSymbols: boolean,
    outputDirectory: string,
    sourceSymbols: boolean,
    sourceSymbolsDirectory: string,
    outputKeepSourceHierarchy: boolean,
}

export interface AssemblerSettings {
    jar: string,
    main: string,
    option: AssemblerOptions,
}

export interface EditorSettings {
    showNotifications: boolean,
    switchProblemsTabOnError: boolean,
	tabAfterInstruction: boolean,
}

export interface EmulatorSettings {
    runtime: string,
    options: string,
    start: string,
}

export interface DebuggerSettings {
    runtime: string,
    options: string,
    startup: string
}
export interface Settings {
    assembler: AssemblerSettings,
    java: JavaSettings,
    valid: boolean,
    editor: EditorSettings,
    emulator: EmulatorSettings,
    debugger: DebuggerSettings,
    outputDirectory: string,
    autoAssembleTrigger: string,
    autoAssembleTriggerDelay: number,
    debuggerDumpFile: boolean,
    byteDumpFile: boolean,
    completionParameterPlaceholders: boolean,
    fileTypesBinary: string,
    fileTypesSid: string,
    fileTypesPicture: string,
    fileTypesSource: string,
    fileTypesC64: string,
    fileTypesText: string,
    opcodes:SettingsOpcodes,
    assemblerLibraryPaths: string[],
    startup: string,
    codeSneakPeekLines: number,
}

export default class SettingsProvider extends Provider {

    private settings:Settings;
    private kickAssemblerLatestVersion:string = "5.25";
    private kickAssemblerWebsite: string = "http://theweb.dk/KickAssembler/";
    private javaMinVersion:string = "11";
    private javaWebsite: string = "https://openjdk.java.net/install/";
    constructor(connection:Connection, projectInfo:ProjectInfoProvider) {

        super(connection, projectInfo);
        // connection.console.info("[SettingsProvider]");

        connection.onDidChangeConfiguration((change:DidChangeConfigurationParams) => {
            const settings = <GlobalSettings>change.settings;
            this.process(<Settings>settings['kickassembler']);
        });
    }

    public getSettings() {
        return this.settings;
    }

    private process(settings:Settings) {
        
        this.settings = settings;
        this.settings.valid = this.validateSettings(settings);

    }

    /**
     * Returns true if the settings for the extension are Valid, 
     * false otherwise.
     * 
     * @param settings 
     */
    private validateSettings(settings:Settings):boolean|undefined {

        // check for java runtime
        let _java_runtime: string = settings.java.runtime;

        if (!PathUtils.fileExists(_java_runtime)) {
            this.getConnection().window.showWarningMessage(`Java Path "${_java_runtime}" does not exist.`);
            return false;
        }
        var compareVersions = require('compare-versions');
        let java = spawnSync(_java_runtime, ['-version']);
        let javainfo_data:string = java.output.toString();
        let javaReason = '';
        let javaVersion = javainfo_data.match(/version "(\d+(\.\d+)*)/);
        if(!javaVersion) {
            javaReason = 'Unknown java version.'; 
        } else if(compareVersions.compare(javaVersion[1],this.javaMinVersion,"<")) {
            javaReason = `Java ${javaVersion[1]} should be updated.`; 
        }
        if(javaReason!=='') {
            var offerJavaDownload = this.getConnection().window.showWarningMessage(javaReason + ` Kick Assembler needs Java >= ${this.javaMinVersion}.`, {
                title: 'Download Update' 
            });
            offerJavaDownload.then((value) => {
                if (value){
                    opn(this.javaWebsite);
                }
            }); 
        }
        // get the assembler jar
        let _assembler_jar = settings.assembler.jar;

        // does the assembler jar filenmame exist?
        if (!PathUtils.fileExists(_assembler_jar)) {
			console.error("Could Not Find the KickAss Jar in the `assembler.jar` setting.");
			return false;
		}

        for(let i=0, il=settings.assemblerLibraryPaths.length; i < il; i++){
            let libPath = settings.assemblerLibraryPaths[i];
            if (path.isAbsolute(libPath) && !PathUtils.directoryExists(libPath)) {
                this.getConnection().window.showWarningMessage(`KickAssembler Library Path "${libPath}" does not exist. Ignoring.`);
            }
        }
        var cpPlugins:string[] = settings.java.plugin.list;

        for(let i=0, il=cpPlugins.length; i < il; i++){
            if (!PathUtils.fileExists(cpPlugins[i])) {
                this.getConnection().window.showWarningMessage(`Java Plugin "${cpPlugins[i]}" does not exist. Ignoring.`);
            }
        }

        if (path.isAbsolute(settings.startup)) {
            this.getConnection().window.showErrorMessage(`Invalid absolute startup path. Enter a filename only.`);
        }
        
        let _tempfile = path.join(os.tmpdir(), "test.asm");
        // console.log(_tempfile);
        const url = require('url');
        let _url = url.pathToFileURL(_tempfile);

        try {

            fs.accessSync(PathUtils.getPathFromFilename(_tempfile), fs.constants.W_OK);
            let assembler = new Assembler();
            let assemblerResults = assembler.assemble(this.settings, _url.href, '', true, true, true);
            var kickassVersion = assemblerResults.assemblerInfo.getAssemblerVersion();
            //console.log(assemblerResults.assemblerInfo);

            if(kickassVersion === "0") {
                // version lower than 5.12, parse output
                var parsedKickassVersion = assemblerResults.stdout.match(/\d+\.\d+/);
                if(parsedKickassVersion) {
                    kickassVersion = parsedKickassVersion[0];
                }
            }

            if(compareVersions.compare(kickassVersion,"4","<")) {
                this.kickAssBelow4Error(kickassVersion);
                return false;       
            }

            if(compareVersions.compare(kickassVersion,"5","<")) {
                var offerKickassDownload = this.getConnection().window.showWarningMessage(`Your KickAssembler Version ${kickassVersion} is outdated.`, {
                    title: 'Upgrade KickAssembler'
                });
                offerKickassDownload.then((value) => {
                    if (value){
                        opn(this.kickAssemblerWebsite);
                    }
                });   
            }        
            else if(compareVersions.compare(kickassVersion,this.kickAssemblerLatestVersion,"<")) {
                if (settings.editor.showNotifications) {
                    var offerKickassDownload = this.getConnection().window.showInformationMessage(`Your KickAssembler Version ${kickassVersion} can be updated to ${this.kickAssemblerLatestVersion}.`, {
                        title: 'Download Update' 
                    });
                    offerKickassDownload.then((value) => {
                        if (value){
                            opn(this.kickAssemblerWebsite);
                        }
                    }); 
                }
            }
        }
        catch (err) {

            // at least try to guess the version by jar size
            const jarFileStats = fs.statSync(_assembler_jar);

            // Kickass 2.x and 3.x are smaller than 400k in size - LOL
            if (jarFileStats.size < 400000) {
                this.kickAssBelow4Error('lower than 4.0')
                return false;
            }

            // log the error and return message that we cannot figure it out :)
            console.log(err);
            this.getConnection().window.showWarningMessage('Unable to Determine KickAssembler Version.');
        }

        return true;
    }

    private kickAssBelow4Error(kickassVersion:string){

        var offerKickassDownload = this.getConnection().window.showErrorMessage(`Your KickAssembler Version ${kickassVersion} is not supported.`, {
            title: 'Get supported KickAssembler Version',
        });

        offerKickassDownload.then((value) => {
            if (value){
                opn(this.kickAssemblerWebsite);
            }
        });
    }

}