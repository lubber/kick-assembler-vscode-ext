/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

export default class NumberUtils {

	public static asDecimal(value:number):string {
		return value.toString(10);
	}

	public static asBinary(value:number):string {
		return "%" + value.toString(2);
	}

	public static asOctal(value:number):string {
		return "0" + value.toString(8);
	}

	public static asHexa(value:number):string {
		return "$" + value.toString(16);
	}

	/**
	 * Takes a String and Tries to convert it
	 * to a Decimal value.
	 * 
	 * @param value the String to Convert
	 */
	public static toDecimal(value:string):number 
	{
		var isImmediate = value.slice(0, 1) == "#";

		if (isImmediate) 
		{
			value = value.slice(1)
		}

		//var numberPosition = isImmediate ? 1 : 0;
		let numberPosition = 0;

		var isLowByte = value.slice(numberPosition, 1) == "<";
		var isHighByte = value.slice(numberPosition, 1) == ">";

		if (isLowByte || isHighByte) {
			// numberPosition++;
			value = value.slice(1)
		}
		var numberSystem = value.slice(numberPosition, 1);

		var numberBase = 10;
		if (numberSystem == "$") {
			numberBase = 16;
			numberPosition++;
		}
		if (numberSystem == "%") {
			numberBase = 2;
			numberPosition++;
		}					

		var num = parseInt(value.slice(numberPosition), numberBase);
		if (!isNaN(num)) {
			if(isLowByte) {
				num = num % 256;
			}
			if(isHighByte) {
				num = num >> 8;
			}  
			return num;
		}
		return undefined;
	}

}