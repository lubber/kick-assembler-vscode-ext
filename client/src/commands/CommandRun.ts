/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/
import { workspace, ExtensionContext, WorkspaceConfiguration, TextDocument} from 'vscode';
import * as vscode from 'vscode';
import * as path from 'path';
import ClientUtils from '../utils/ClientUtils';
import ProcessUtils, { ProcessInfo } from '../utils/ProcessUtils';
import PathUtils from '../utils/PathUtils';

export class CommandRun {

    private configuration: WorkspaceConfiguration;
    private showDiagnostics : boolean = false;
    private processUtils: ProcessUtils;
    private output: vscode.OutputChannel

    public buildFilename : string;
    public viceSymbolFilename : string;
    public symbolFilename : string;

    constructor(context: ExtensionContext, output: vscode.OutputChannel) {
        this.configuration = workspace.getConfiguration('kickassembler');
        this.showDiagnostics = this.configuration.get("editor.showDiagnostics");
        this.buildFilename = null;
        this.viceSymbolFilename = null;
        this.symbolFilename = null;
        this.processUtils = new ProcessUtils(output);
        this.output = output;
    }

    public runOpen(text: TextDocument) {


        // let _base2 = path.basename(text.fileName)
        let _base2 = this.buildFilename;
        let _output_path = ClientUtils.GetOutputPath(this.output);
        // let _output_file = path.join(ClientUtils.GetOutputPath(), ClientUtils.CreateProgramFilename(_base2));
        let _output_file = path.join(ClientUtils.GetOutputPath(this.output), _base2);

        let _root_path = workspace.workspaceFolders[0];
        let _root_path_len = _root_path.uri.fsPath.length
        let _file_path = path.dirname(text.fileName)
        let _out_sub_part = _file_path.substring(_root_path_len)

        // _output_file = path.join(_output_path, _out_sub_part, ClientUtils.CreateProgramFilename(_base2));
        let keepSourceHierarchy = this.configuration.get("assembler.option.outputKeepsSourceHierarchy", false);
        if (keepSourceHierarchy) {
            _output_file = path.join(_output_path, _out_sub_part, _base2);
            _output_path = path.dirname(_output_file)
        }
        
        this.buildFilename = _output_file;
        // this.symbolFilename = path.join(_output_path, this.symbolFilename);
        // this.viceSymbolFilename = path.join(_output_path, this.viceSymbolFilename)

        // new output path stuff
        this.output.appendLine("----------------------------------------")
		const newSourcePath = PathUtils.GetPathFromFilename(text.uri.fsPath);
		this.output.appendLine(`Source Path:  ${newSourcePath}`);
		const newOutputDir:string = this.configuration.get("assembler.option.outputDirectory");
		this.output.appendLine(`Output Dir:   ${newOutputDir}`);
		const newOutputKeepHier:boolean= this.configuration.get("assembler.option.outputKeepsSourceHierarchy", false);
		this.output.appendLine(`Output Keep:  ${newOutputKeepHier}`);
        let newOutputPath = PathUtils.getOutputPath(ClientUtils.getWorkspaceFolderPath(), newSourcePath, newOutputDir, newOutputKeepHier)
		this.output.appendLine(`Output Path:  ${newOutputPath}`);
        let newOutputFilename = _output_file;
		this.output.appendLine(`Output File:  ${newOutputFilename}`);

        if (this.symbolFilename != undefined) {
            let newSymbolFile = path.join(newOutputPath, this.symbolFilename);
		    this.output.appendLine(`Symbol File:  ${newSymbolFile}`);
            this.symbolFilename = newSymbolFile;
        }

        if (this.viceSymbolFilename != undefined) {
            let newViceSymbolFile = path.join(newOutputPath, this.viceSymbolFilename);
		    this.output.appendLine(`Vice File:    ${newViceSymbolFile}`);
            this.viceSymbolFilename = newViceSymbolFile;
        }
        
        this.output.appendLine("----------------------------------------")

        this.run(newOutputFilename, newOutputPath, text);

        return


        this.buildFilename = this.buildFilename.trim();

        /*
            when the path on the filename is not
            absolute then we want to add the
            source folder to it
        */

        if (!path.isAbsolute(this.buildFilename)) {
            this.buildFilename = ClientUtils.GetOutputPath(this.output)  + path.sep + this.buildFilename;
        }

        this.buildFilename = path.normalize(this.buildFilename);

        let _base = path.basename(this.buildFilename);
        let _path = path.dirname(this.buildFilename);

        /*
            fix vice symbol filename
        */

        if (this.viceSymbolFilename) {
            
            if(!path.isAbsolute(this.viceSymbolFilename)) {
                this.viceSymbolFilename = ClientUtils.GetOutputPath(this.output) + path.sep + this.viceSymbolFilename;
            }
    
            this.viceSymbolFilename = path.normalize(this.viceSymbolFilename);
        }

        if (this.buildFilename) {
            this.run(this.buildFilename, _path, text);
        } else {
            let program = path.join(ClientUtils.GetOutputPath(this.output), ClientUtils.CreateProgramFilename(_base));
            this.run(program, _path, text);
        }
        
    }

    public runStartup(text: TextDocument) {

        this.buildFilename = this.buildFilename.trim();

        /*
            when the path on the filename is not
            absolute then we want to add the
            source folder to it
        */

        if (!path.isAbsolute(this.buildFilename)) {
            this.buildFilename = ClientUtils.GetOutputPath(this.output)  + path.sep + this.buildFilename;
        }

        this.buildFilename = path.normalize(this.buildFilename);

        let _base = path.basename(this.buildFilename);
        let _path = path.dirname(this.buildFilename);

        /*
            fix vice symbol filename
        */

        if(!path.isAbsolute(this.viceSymbolFilename)) {
            this.viceSymbolFilename = ClientUtils.GetOutputPath(this.output) + path.sep + this.viceSymbolFilename;
        }

        this.viceSymbolFilename = path.normalize(this.viceSymbolFilename);

        if (this.buildFilename) {
            this.run(this.buildFilename, _path, text);
        } else {
            let program = path.join(ClientUtils.GetOutputPath(this.output), ClientUtils.CreateProgramFilename(_base));
            this.run(program, _path, text);
        }
    
    }

    public run(buildFilename:string, sourcePath:string, text: TextDocument, runMode:string = "open") {

        //  get emulator runtime
        let emulatorRuntime: string = this.configuration.get("emulator.runtime");

        if (!emulatorRuntime) {
            emulatorRuntime = this.configuration.get("emulatorRuntime");
        }

		// check for runtime setting in source file

		var _text = text.getText();
		var _pos = _text.indexOf('@kickass:emulator.runtime=')
		if (_pos > 0) {
			// matching end @
			var _endpos = _text.indexOf('\n', _pos + 1)
			if (_endpos > 0) {
				var _runtime = _text.substring(_pos, _endpos - 1)
				var _split = _runtime.split('=')
				emulatorRuntime = _split[1]
			}
		}

        // normalize path fix slashes, etc.
        emulatorRuntime = path.normalize(emulatorRuntime);

        // remove trailing slash
        if (emulatorRuntime.endsWith(path.sep)) {
            emulatorRuntime = emulatorRuntime.substring(0, emulatorRuntime.length - 1);
        }

        let _emulator_options_str: string = this.configuration.get("emulator.options");
        if (!_emulator_options_str) {
            _emulator_options_str = this.configuration.get("emulatorOptions");
        }

		// check for options setting in source file
		var _text = text.getText();
		var _pos = _text.indexOf('@kickass:emulator.options=')
		if (_pos > 0) {
			// matching end @
			var _endpos = _text.indexOf('\n', _pos + 1)
			if (_endpos > 0) {
				var _runtime = _text.substring(_pos, _endpos)
				var _split = _runtime.split('=')
				_emulator_options_str = _split[1]
			}
		}

        if (!_emulator_options_str) {
            _emulator_options_str = "";
        }

        let emulatorOptions: string[] = _emulator_options_str.match(/\S+/g) || [];

        // vice specific options
        let _vsf = "";
        let _use_vice_symbols: boolean = false; ;
        _use_vice_symbols = this.configuration.get("assembler.option.viceSymbols") || this.configuration.get("emulatorViceSymbols");

        if (_use_vice_symbols) {
            if (this.viceSymbolFilename) {
                _vsf = this.viceSymbolFilename;
            }
        }

        // enclose in quotes to accomodate filenames with spaces on Mac
        if (process.platform == "darwin") {
            if (emulatorRuntime.search(" ") > 0) { emulatorRuntime = ClientUtils.EncloseWithQuotes(emulatorRuntime); }
            if (buildFilename.search(" ") > 0) { buildFilename = ClientUtils.EncloseWithQuotes(buildFilename); }
            if (_vsf.search(" ") > 0) { _vsf = ClientUtils.EncloseWithQuotes(_vsf); }
        }

        // handle replaceable variables
        ClientUtils.ReplaceOptionVar("${kickassembler:buildFilename}", buildFilename, emulatorOptions);
        ClientUtils.ReplaceOptionVar("${kickassembler:viceSymbolsFilename}", _vsf, emulatorOptions);

        this.output.appendLine("")
        this.output.appendLine("Replacement Variables Created:")
        this.output.appendLine("")
        this.output.appendLine(`  {kickassembler:buildFilename}:        ${buildFilename}`);
        this.output.appendLine(`  {kickassembler:viceSymbolFilename}:   ${_vsf}`);
        this.output.appendLine("")

        // finalize options string
        let args = emulatorOptions.filter(function (el) {
            return el != "";
        })

        const processInfo = new ProcessInfo({
            command: emulatorRuntime,
            args: args,
            currentWorkingDirectory: sourcePath,
            debugMode: runMode,
            showDiagnostics: this.showDiagnostics
        });
        
        this.processUtils.spawnExternalProcess(processInfo);
    }
}