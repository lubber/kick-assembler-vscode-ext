/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { spawnSync } from 'child_process';
import { workspace, window, ExtensionContext, WorkspaceConfiguration, TextDocument, Extension, extensions } from 'vscode';
import PathUtils from '../utils/PathUtils';  
import * as vscode from 'vscode';
import * as path from 'path';
import ClientUtils from '../utils/ClientUtils';
import ProcessUtils, { ProcessInfo } from '../utils/ProcessUtils';

export class BuildInformation {
    public buildStatus: number
    public buildData: string
    public buildError: vscode.Diagnostic[] = []
    public buildErrorFile: string
}

export class CommandBuild { 

    public stdout: string;
    
    private configuration: WorkspaceConfiguration;
    private output: vscode.OutputChannel;
    private showOutput : boolean = true;
    private showDiagnostics : boolean = false;
    private extension : Extension<any>;
    private processUtils: ProcessUtils;

    constructor(context:ExtensionContext, output: vscode.OutputChannel, showOutput : boolean = true) {
        this.configuration = workspace.getConfiguration('kickassembler');
        this.output = output;
        this.showOutput = showOutput;
        this.showDiagnostics = this.configuration.get("editor.showDiagnostics");
        this.extension = extensions.getExtension('paulhocker.kick-assembler-vscode-ext');
        this.processUtils = new ProcessUtils(output);
    }

    public buildStartup(text:TextDocument, showOutput:boolean = true): BuildInformation | undefined {

        var _textDocument : TextDocument = text;
        
        if (!_textDocument) {

            var nostartup = window.showWarningMessage(`Cannot build because there is no startup file defined in your Settings.`, { title: 'Open Settings'});

            nostartup.then((value) => {
                if (value){
                    vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.startup`);
                }
            });

            return;
        }

        // return this.build(uri);
        return this.build(_textDocument, showOutput, "startup");
    }

    public build(text:vscode.TextDocument, showOutput:boolean = true, buildMode:string="open"): BuildInformation {

        // show notifications setting
        let _show_notifications : boolean = this.configuration.get("editor.showNotifications");

        // get the java runtime
        let _java_runtime: string = this.configuration.get("java.runtime");

        // get the path to the kickass jar
        let _assembler_jar: string = this.configuration.get("assembler.jar");

        // get the assembler main class
        let _assembler_main: string = this.configuration.get("assembler.main");

        // this.output.appendLine(`show notif: ${_show_notifications}`)
        // this.output.appendLine(`java runtime: ${_java_runtime}`)
        // this.output.appendLine(`assembler jar:${_assembler_jar}`)
        // this.output.appendLine(`assembler main:${_assembler_main}`)
        // this.output.appendLine(`text filename:${text.fileName}`)

        let base = path.basename(text.fileName);

        let _output_path = ClientUtils.GetOutputPath(this.output);

        let outputFile = path.join(ClientUtils.GetOutputPath(this.output), ClientUtils.CreateProgramFilename(base));

        // new output path stuff
        this.output.appendLine("----------------------------------------")
		const newSourcePath = PathUtils.GetPathFromFilename(text.uri.fsPath);
		this.output.appendLine(`Source Path:  ${newSourcePath}`);
		const newOutputDir:string = this.configuration.get("assembler.option.outputDirectory");
		this.output.appendLine(`Output Dir:   ${newOutputDir}`);
		const newOutputKeepHier:boolean= this.configuration.get("assembler.option.outputKeepsSourceHierarchy", false);
		this.output.appendLine(`Output Keep:  ${newOutputKeepHier}`);
        let newOutputPath = PathUtils.getOutputPath(ClientUtils.getWorkspaceFolderPath(), newSourcePath, newOutputDir, newOutputKeepHier)
		this.output.appendLine(`Output Path:  ${newOutputPath}`);
        let newOutputFilename = path.join(newOutputPath, ClientUtils.CreateProgramFilename(path.basename(text.fileName)));
		this.output.appendLine(`Output File:  ${newOutputFilename}`);
        this.output.appendLine("----------------------------------------")

        let _root_path = workspace.workspaceFolders[0];
        let _root_path_len = _root_path.uri.fsPath.length
        let _file_path = path.dirname(text.fileName)
        let _out_sub_part = _file_path.substring(_root_path_len)

        outputFile = path.join(_output_path, _out_sub_part, ClientUtils.CreateProgramFilename(base));
        _output_path = path.dirname(outputFile)

        // ??remove old program file
        // PathUtils.fileRemove(outputFile);

        var sourceFile = text.fileName;
        var sourcePath = path.dirname(text.fileName);

        var cpSeparator:string = process.platform == "win32" ? ';' : ':';
        var cpPlugins:string[] = this.configuration.get("java.plugin.list");
        var cpPluginParameters:string[] = this.configuration.get("java.plugin.properties");
        cpPluginParameters = cpPluginParameters.map(p => '-D' + p.trim());

        // add custom java options from settings
        let _java_options_raw: string = this.configuration.get("java.options");

        let javaOptions = [
            "-cp", 
            cpPlugins.join(cpSeparator) + cpSeparator + _assembler_jar,
            ...cpPluginParameters,
            _assembler_main, 
            sourceFile, 
            "-odir",
            newOutputPath,
            "-showmem",
        ];

        if (_java_options_raw.
            length > 0) {
            let _raw_split = _java_options_raw.split(" ");
            javaOptions = [..._raw_split, ...javaOptions];
        }

        if (this.configuration.get("debuggerDumpFile")){
            javaOptions.push('-debugdump');
        }

        if (this.configuration.get("byteDumpFile")){
            let byteDumpFile = path.join(ClientUtils.GetOutputPath(this.output), "ByteDump.txt");
            javaOptions.push('-bytedumpfile', byteDumpFile);
        }

        if (this.configuration.get("assembler.option.viceSymbols") || this.configuration.get("emulatorViceSymbols")){
            javaOptions.push('-vicesymbols');
        }
        if (this.configuration.get("assembler.option.sourceSymbols") ){
            javaOptions.push('-symbolfile');
            let symbolDir:string = this.configuration.get("assembler.option.sourceSymbolsDirectory");
            symbolDir = symbolDir.trim();
            if (symbolDir !== ''){
                javaOptions.push('-symbolfiledir',symbolDir);
            }
        }

        if (this.configuration.get("assembler.option.afo")){
            javaOptions.push('-afo');
        }

        if(this.configuration.get("opcodes.DTV")){
            javaOptions.push('-dtv');
        }

        if(!this.configuration.get("opcodes.illegal")){
            javaOptions.push('-excludeillegal');
        }

        if(this.configuration.get("assembler.option.binfile")) {
            javaOptions.push('-binfile');
        }

        // KickAss ignores a non existing cfg file, so it's safe to simply always add it
        let cfgFile:string = this.configuration.get("assembler.option.cfgfile");
        cfgFile = cfgFile.trim();
        if(cfgFile !== '') {
            javaOptions.push('-cfgfile', cfgFile);
        }

        // add library paths for kick assembler
        var libdirPaths:string[] = this.configuration.get("assemblerLibraryPaths");

        libdirPaths.forEach((libPath) => {

            if(!path.isAbsolute(libPath)) {
                libPath = path.join(sourcePath, libPath);
            }

            if (PathUtils.directoryExists(libPath)) {
                javaOptions.push('-libdir',libPath);
            }
        });

        if (showOutput) {
            this.output.show(true);
        } else {
            javaOptions.push('-nooutput'); 
        }

        // show diagnostic information
        if (this.showDiagnostics && showOutput) {
            this.output.appendLine("");
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine(`${this.extension.packageJSON.displayName} ${this.extension.packageJSON.version}`);
            this.output.appendLine("Build Diagnostics");
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine(`Mode             : ${buildMode}`);
            this.output.appendLine(`Platform         : ${process.platform}`);
            this.output.appendLine(`Java Runtime     : ${_java_runtime}`);
            this.output.appendLine(`Java Options     :`);
            this.output.appendLine("");
            for (var i = 0; i < javaOptions.length; i++) {
                this.output.appendLine(`  ${javaOptions[i]}`);
            }
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine("");
            }


        var start = process.hrtime();

        let java = spawnSync(_java_runtime, javaOptions, { cwd: path.resolve(sourcePath)});

        var end = process.hrtime(start);

        let errorCode = java.status;

        let time = `(${end[0]}s ${end[1].toString().substring(0,3)}ms)`;

        var errorText: RegExpMatchArray | null,
            errorPosition: RegExpMatchArray | null;

        let _java_output = java.stdout.toString();

        if (errorCode > 0) {

            console.error(java.stderr.toString());
            let _error_output = java.stdout.toString();
            errorText = _error_output.match(/Error: (.*)/);
            errorPosition = _error_output.match(/at line (\d+), column (\d+) in (.*)/);

            if (showOutput) {
                window.showWarningMessage(`Build of ${base.toUpperCase()} Failed ${time}`);
            }

        } else {
            
            if (_show_notifications && showOutput) {
                window.showInformationMessage(`Build of ${base.toUpperCase()} Complete ${time}`);
            }
        }

        if (this.showOutput) {
            this.output.append(_java_output);
        }

        this.stdout = _java_output;

        var _info = new BuildInformation();
        _info.buildStatus = java.status;
        _info.buildData = java.output.toString();

        if(errorText && errorPosition && errorText.length > 0 && errorPosition.length > 2) {

            let errorMessage = errorText[1];
            let errorLine = parseInt(errorPosition[1],10) - 1;
            let errorColumn = parseInt(errorPosition[2],10) - 1;
            _info.buildErrorFile = errorPosition[3];
            _info.buildError.push({
                severity: vscode.DiagnosticSeverity.Error,
                range: new vscode.Range(
                    new vscode.Position(errorLine,errorColumn),
                    new vscode.Position(errorLine,errorColumn),
                ),
                message: errorMessage,
                source: "kickassembler",
            });
        }

        //fetch all warnings
        let warnings = _java_output.match(/(\(.*) (\d+:\d+)\) Warning: (.*)/g) || [];

        warnings.forEach((warningString) => {
            // split up into details
            let warning = warningString.match(/(\(.*) (\d+:\d+)\) Warning: (.*)/);
            let warningPosition = warning[2].split(':');
            let warningLine = parseInt(warningPosition[0],10) - 1;
            let warningColumn = parseInt(warningPosition[1],10);
            _info.buildError.push({
                severity: vscode.DiagnosticSeverity.Warning,
                range: new vscode.Range(
                    new vscode.Position(warningLine,warningColumn),
                    new vscode.Position(warningLine,warningColumn),
                ),
                message: warning[3],
                source: "kickassembler",
            });
        });
                

        return _info;
    }

    public buildCartridgeFile(buildFilename: string): string {
        let cartridgeConverterRuntime: string = this.configuration.get("cartridgeConverter.runtime");

        if (!cartridgeConverterRuntime) {
            return buildFilename;
        }

        // normalize path fix slashes, etc.
        cartridgeConverterRuntime = path.normalize(cartridgeConverterRuntime);

        // remove trailing slash
        if (cartridgeConverterRuntime.endsWith(path.sep)) {
            cartridgeConverterRuntime = cartridgeConverterRuntime.substring(0, cartridgeConverterRuntime.length - 1);
        }

        let optionsRaw: string = this.configuration.get("cartridgeConverter.options");
        let options: string[] = [];
        if (optionsRaw.length > 0) {
            let rawSplit = optionsRaw.split(" ");
            options = [...rawSplit];
        }

        let cartridgeFilename = buildFilename;
        const extension = cartridgeFilename.slice(cartridgeFilename.lastIndexOf('.'))
        cartridgeFilename = cartridgeFilename.replace(extension, '.crt');

        const outputPath = ClientUtils.GetOutputPath(this.output);
        const cartridgeFullFilename = path.join(outputPath, cartridgeFilename);
        PathUtils.fileRemove(cartridgeFullFilename);

        ClientUtils.ReplaceOptionVar("${kickassembler:buildFilename}", buildFilename, options);
        ClientUtils.ReplaceOptionVar("${kickassembler:cartridgeFilename}", cartridgeFilename, options);

        this.output.appendLine("")
        this.output.appendLine("Replacement Variables Created:")
        this.output.appendLine("")
        this.output.appendLine(`{kickassembler:buildFilename}:  ${buildFilename}`);
        this.output.appendLine(`{kickassembler:cartridgeFilename}:  ${cartridgeFilename}`);
        this.output.appendLine("")

        const processInfo = new ProcessInfo({
            command: cartridgeConverterRuntime,
            args: options,
            currentWorkingDirectory: outputPath,
            debugMode: 'open',
            showDiagnostics: this.showDiagnostics
        });

        this.processUtils.spawnSyncExternalProcess(processInfo, () => !PathUtils.fileExists(cartridgeFullFilename));

        return cartridgeFilename;
    }
}
