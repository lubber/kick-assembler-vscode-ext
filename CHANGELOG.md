# CHANGELOG

<!--- next entry here -->

## 0.23.3
2024-04-25

### Fixes

- **build:** display replacement variables (014eb41a868110ffb9ceb84f353b64c719c45507)

## 0.23.2
2024-04-03

### Fixes

- **build:** remove old log messages (480046f95fdc3785ce9026cd6ab7c636ed9bd185)
- **run:** incorrect filename when starting cartridge (97e2965071ff39fb6d2a45309e5afad559f82635)

## 0.23.1
2024-02-15

### Fixes

- remove some old debug comments (1f610854b4dbfbe60f846c74af9ab728e519998c)

## 0.23.0
2024-02-10

### Features

- bump to next version (d7e9dcf53df5096d0faaff7549b0f4e94cf93113)

## 0.22.0
2023-07-18

### Features

- **client:** support for building cartridges (a2da0182e56e9c099444bac85a3d4ea21d1a672b)

### Fixes

- **editor:** reference and definition providers not functioning correctly (322850000ba18c29208153023f346c4453e90c4f)
- **runner:** spawning on macos was broken for .app (df180f64962e66abacb449a43db04ed7c648d25f)
- **runner:** linux not spawning runtime (12f049e0d3dc84345a3316af5a81026bf6498286)

## 0.21.0
2023-03-23

### Features

- **client:** add inline settings for emulator runtime and options (cfb39f34166ce6c7b0ac649df10e43d831d9c7ac)
- **client:** handle inline settings for startup builds (c556c107217157ad3cdb4359a58ce8695cd3beed)

### Fixes

- **editor:** suppress startup file messages (8f37167cbffad4f48ad2182280e9db2b452c5bd5)
- **completion:** provider was not showing directives when there was no object created (07d4a112e13ab4229320b146a6834669f88e692e)
- **completion:** cleanup commented out code (f7282b57816db9ae339f7a4903b8a8feadc8908a)
- **project:** hover and completion broken when editing file not in startup project (193a95a1bfd938593d5867cc71292fc6d8f4d8ef)

## 0.20.7
2023-03-09

### Fixes

- **completion:** duplicate global pseudo-commands (d817349833c22fae636f7e08234cfa9fc8236c17)

## 0.20.6
2023-01-05

### Fixes

- **hover:** issue #155 hover not correct on decimal literals (c6b3729d555ac257afd7b0af930df61b85b5b4b2)

## 0.20.5
2022-12-24

### Fixes

- **ci:** add publishing to open vscode marketplace (c03c0bb90b7173968fd1b1903b7259410148536c)

## 0.20.4
2022-12-18

### Fixes

- **readme:** vsmarketplace badges stopped working (0b32719529db17976f8dda72b33f082aaf1cc239)

## 0.20.3
2022-12-17

### Fixes

- **project:** code cleanup (48114df6c71d1bdd4ecd147c9b5d233a39dbc17a)
- **project:** reduce number of calls to get scoped lines (520fa1b64673a3abbf63185ce7726ee484219dd5)
- **assemble:** reduce number of compiles when auto assemble enabled (2d062be67a33b3f91b344d2525ee2ea802385802)
- **debug:** handle when no options specified (486266c4a954c9d857821148929daa481b9c763a)
- **emulator:** handle when no options specified (646ee6b5f42a09cdeb2c6eed6d12abd6e65d7df2)

## 0.20.2
2022-11-28

### Fixes

- **editor:** better open/close bracket scope check (a701c3e2025ee893ec5a7184e5550542e5c67a4d)

## 0.20.1
2022-11-23

### Fixes

- **editor:** force assemble on build and open (44658df5f6b6c2cf086969061b0cf1876f54a663)
- **language:** add missing opcode constants (bc1ddf4c17254fd9f369ea9f97a5973c21324040)

## 0.20.0
2022-11-19

### Features

- **settings:** check latest kickass 5.25 (cf7e00550616368e3d674d302abee2d7ceadab77)

## 0.19.1
2022-11-12

### Fixes

- **editor:** issue #102 (361a4dcdf87b1d7b935c3448f838f89579f29812)

## 0.19.0
2022-10-29

### Features

- **client:** create/show a status bar message (instead of a information popup) upon successful set up (ae109921cfbb97bea66a2a6a17614dbaf810962d)
- **client:** ignore debugger setting errors on startup (3c8b2c4d57b22fcd3b590ec28f4f54fea3883b4f)

## 0.18.0
2022-09-24

### Features

- new completion provider (6fc884480c559fc23eba0b99c807464734f5ca97)

## 0.17.4
2022-07-03

### Fixes

- **documentsymbols:** remove duplicate references and fix anonymous labels (89169c34a37e6d6c1748e80f8c6ce66b57692083)
- **editor:** issues for #147 (391148815ac2c01c7fc1da8e6abf1720fbd1091a)

## 0.17.3
2022-06-18

### Fixes

- **completion:** broken completion items on edit (c4cb30804ea324e555595173dbcbdb0556b94cc1)

## 0.17.2
2022-06-13

### Fixes

- replace substr with slice (7c25bb2c304e5177e9b7c5cb09831b67a86f4d6f)

## 0.17.1
2022-05-06

### Fixes

- **setting:** removed old settings (206d7fe2bffb494bcb94ded49e812d2ceef55773)

## 0.17.0
2022-04-28

### Features

- **settings:** enable/disable insertion of .break (248966757759c035ef1077aed1c1009863facb7c)

## 0.16.0
2022-01-06

### Features

- **build:** use -noouput feature of kickass 5.24 (348ff56daa69c290b72f5ccb428e3156ac3da5d7)

### Fixes

- **client:** add version information to diagnostics (9c3df07dbda7bc3c2780b13388bdec04f0beb4a8)

## 0.15.0
2022-01-01

### Features

- **settings:** Added a tabAfterInstruction setting to support using tab after instructions. Also fixed the stringutils to support tabs. (331b104d3ec97007fcda65305ebaa0ceedb470bb)
- **setting:** error on absoilute startup path (cbc95de96cc988be60c8b3cd268c7a84de40dd62)

### Fixes

- **client:** handle trailing slashes for run and debug (c1c5113eb0069a0cdff3306a01dfa4793b85d797)
- **build:** dont always create source.asm (fdee86d119402c7eda5c2b14ccb153531987f707)
- **run/debug:** vice symbols using source filename (f79f4429c739f69f7b3a1b6b8ae970707ff9fe21)
- **editor:** honor closed viewers until action invoked (edceea8ec1d8a1c4793c7675c4426850c9fe6126)
- **ci:** use 2.5.3 until bug resolved in 2.6.0 (e8f1fbf7fa707d7ad60161d3203505fc4d0f4f2d)
- **editor:** ensure view is opened when output requested (529c468bc8a227bbb5dbe3c0af9c7469a9ebe7cd)

## 0.14.0
2021-12-28

### Features

- **setting:** check java version < 11 (ae436287b965cc41b53070f7b1ddc6e175db344b)
- **setting:** hint for defaults in kickass.cfg (326f8e67f87e1e4242bf750ea6a53ef209b6d3ff)

### Fixes

- **settings:** better java version regex (ce2826b063fb1698be5510d2943a3074d71c8fef)
- **build:** make sure showmem is always used (e5d35cf4e8ab56552aa40db4f0529b0c73406b6b)

## 0.13.0
2021-12-25

### Features

- **build:** use -odir option when output directory specified (a2cd0ff24a6a4f43d0cca9aeb85c3c6ee0c78327)

### Fixes

- **run:** use output directory for filename (19d0cb84f3a288666ab8698987caf01896b7e005)
- **client:** remove vice symbols from emulator (9bee3794bb0f535525d816ecd7df0014a3b6ccf3)
- **client:** fix command line using replacement vars (93441a91697c5b4af3310a60680a7ba9c17f6a2f)
- **run:** paths for posix client broken (59879b7be65be3cb2a360af285e5b05e49ad1f61)
- **run:** handle enclosed paths on mac (d5fa70fd0e412deee7ed33ef4321aaa3ab51ade2)
- **run:** ignore old vice symbols (60cff33a73a4d8ce36d92d7207107dd5502d969b)
- use new markdown description for all settings (c34f1dfa370324dd18b1392fdb6ba5b7952f8896)
- **settings:** cleanup fixes for new output path (5c070a44b06103d4c9f879118bb659a7659a5f0b)
- **setting:** typo fix (56e746f891fb81d127038363122e7360ce892623)
- **run:** mac specific space handling (1baea4c9bd73c579130fd2357c0aa0efaa1a9810)
- **run:** fixed running emulator when .app was specified (dbaa1b752bf2dfb0b07ae806812fbee7e03e5f0e)
- **debug:** fix path support with spaces for mac (604e349133bf3916117fca79b734faaea54fe0c0)
- **run:** cleanup code and suppress empty runtime options (c16f59074acd6b0a98d1db1b33164e82fa8c3cbe)

## 0.12.3
2021-12-21

### Fixes

- **settings:** new plugin settings were not used (68726782644007efa1d8b6196fda4da24c8b0250)
- **heading:** change copyright year (729918fdb5179b8a1fc708bd64f500687fff8799)
- **assembler:** check for invalid startup file (6624f1216431f1afc8234197aa7d557c57ba9d99)

## 0.12.2
2021-12-14

### Fixes

- **ci:** updated gitlab semrel (6049ee8c93f2729d3544babe115de0e5ca1d8d5f)
- **run:** build and run not working when output path was different (97f583943f98df0c316db48beb98a4c587d4611b)
- **settings:** output folders not on same drive as workspace caused startup errors (ae9bb61d0c44a4e4900f48d0e4e308ded67db95c)

## 0.12.1
2021-12-13

### Fixes

- **run:** whitespace from buildFileName fails vice (78ff793d78be9ae05b5741412e518db92291ee69)

## 0.12.0
2021-12-12

### Features

- **workflow:** memoryview cache, autoassemle off (72ec0985f1865d734cdd11e19d61017bbf960349)

### Fixes

- **emulator:** support different emulators and debuggers (26807948a8251978125d97091bb7eec62f847715)
- **emulator:** removed startup option (896f883adbcac57c0e4b0e03f67d80e10497f337)
- **run:** change current dir to workspace folder when running (775056cba5ac0f08eeba0864585f36d1d7d3c79c)
- **debug:** change debug to use buildFilename (1ff181c4da7ee6e99758aa1dca080bab12530268)
- **run:** use buildFilename (5bbc33f98b9643423504e1346fc3ebc35ce56fd9)
- **editor:** add diagnostic info and change runtime path (ccdffab9ee26260ec974edf286e419ba6bef7fad)
- **build:** update current dir when debugging (b11396c741fbb711b903364e2249b0157920753b)
- **settings:** added new options to provider (bfee2a107816093787fab1a308e4f3c4714b67ab)
- **ci:** change active length for artifacts (245cbd6fa03727f12e00934d96b85e04405d9524)
- **ci:** increase active length for artifacts (0144ee59ac15fb3ad57dda7d212aa5ce58fe2198)
- **settings:** tempfolder was wrong parameter (11c99433eec8857c2b43182034ff1ca4044289a0)
- **typo:** some name corrections (02a33c9c8f21a7c0086813683f071f9453477b4e)
- **setting:** markdown format and links (7fb683058bf1f23341bf0b516d128e395cea342d)
- **memoryview:** build only on memview visible (7ff22748be3c4e400d2be6a564d46e4f67af5b65)
- **code:** removed a few obsolete files (51a4861df3a5bb9a0258e988753ab92913db73a7)
- **code:** added old history to changelog (fd4e9cd7aa069bc19ae72480d39db8fa663846a6)
- **code:** updated readme and new guides started (75d3188657ea331248a3cb0c273eeb8511e6ab49)

## 0.11.0
2021-11-28

### Features

- **settings:** check kickass 5.23 (acdb85b836fb2df64b343917a916c7dd367344db)

### Fixes

- **build:** auto switch to problems made optional (f4709aebd5e9102ccd4cbefabe69c5d37e6ee307)

## 0.10.0
2021-11-28

### Features

- **dependencies:** upgraded languageserver/client (92c456cd8e55c07b2f06edee8fec6496db47b30a)
- **build:** support external kickass config files (bf4290b999fde96bfd791c991f8430baa9e9e1bc)

### Fixes

- **setting:** change default for config file setting to blank (3b941ac585dcf62c3cf3c6aa5e8caf805b951b58)

## 0.9.1
2021-11-27

### Fixes

- **folding:** assert(error) folding fixed (7a72ef9b9916c1b7b7e9c6e4ee1a1d6941f06758)
- **ci:** use node 14-slim (964764c033176cbca6bc3e86734abf1d07a058cd)

## 0.9.0
2021-10-26

### Features

- **memoryview:** virtual segment color (f42bdaba2e12093db35eef8d16cff1a9c8bde761)

### Fixes

- **memoryview:** rename settings (5a0c7ee67f553cfc535748170520825c2be3b241)
- **memoryviewer:** typo in memory parser (6f2fe2644c141e2e44378282db6cd19bc09b95cb)

## 0.8.6
2021-10-25

### Fixes

- **memoryview:** incorrect calculation for segment (1c881fc30f6521aabaa45c48caf1df3b179c71a7)

## 0.8.5
2021-10-23

### Fixes

- **build:** change verbiage on startup builds (8ac909a80b9a7cd729e14806371ae3419536e194)
- **build:** removed shell spawn on build (5c8fe00fb16fde6a1bfbb98e00028457dced9342)

## 0.8.4
2021-10-21

### Fixes

- **startup:** supress build message on startup (77f4e291cb8a11c55fbd17be2b64e9b3d848314b)

## 0.8.3
2021-10-20

### Fixes

- **ci:** remove other changelog entries (e7cdb4ee00136cb79a5519d5d6d00f094957c20d)
- **memoryview:** suppress build messages (8e2d9409e997c8a86cc4f8f4ea4ca3f325d7d509)

## 0.8.2
2021-10-17

### Fixes

- **build:** memory view was causing multiple builds (9c9ec24ee80dffaf5ee3c40a551c8a8ca918f121)
- **memoryview:** not respecting new opened files (cf57d55a7eb8ea244891c0f5cd8f52b5434b051e)
- **memoryview:** clear view when document closed (f4c2e7d72948dea62810b69abf39c12c434eaacc)

### Other changes

- getting some wierd problems with opening (edaebf771928308998f1d8b5798fa56b83861b98)
- Merge branch 'fix-111-compile-issues' into 'master' (11a49eade4e10bfa34932ebc0f80bb9e42b83c60)

## 0.8.1
2021-09-28

### Fixes

- **memoryview:** show when visible (41ab20bb1df9da6d6d3520ea89e8c68b518276b6)

### Other changes

- Merge branch 'fix-memory-view-vanishes' into 'master' (9c900401d33101bcb41314808ea547d2541320df)

## 0.8.0
2021-09-27

### Features

- **settings:** kickass 5.22 version check support (f740b114413056b69ac58e1e9fe6df816f4a9b54)

### Other changes

- Merge branch 'ka522support' into 'master' (c219432b4d8d12d5ec19837755e0609b97bd6b1f)

## 0.7.17
2021-09-11

### Fixes

- **startup:** validate settings (1efec7ec4d3a9096b02222c6f30aaf801934decd)

### Other changes

- Merge branch 'fix-startup-validation' into 'master' (55976a98a244433c84aa78c1154c96031ea85987)

## 0.7.16
2021-09-08

### Fixes

- **hover:** remove suppression of blank line (e9a6158dbe92691df63fb1f6c97e20ab30ca11fb)

### Other changes

- Merge branch 'enhance-hover-remarks' into 'master' (12024884b2ce54c404f0969c0b1defcea19effde)

## 0.7.15
2021-09-05

### Fixes

- **settings:** support for suppressing informational messages (18ae1ec6b301132256e084b7b125d02841936f1b)

### Other changes

- (fix) support for suppressing informational messages (cf3c11a5165936ee3bcd8c7648783d9224be1ad7)
- Merge remote-tracking branch 'origin/fix-issue-109-supress-messages' into fix-issue-109-supress-messages (1c91701314c8e105139818fc03279dad7a6a1e02)
- Merge branch 'fix-issue-109-supress-messages' into 'master' (71397ff8db5b1ef475e5e82a5c0cff43d36b7748)

## 0.7.14
2021-06-24

### Fixes

- **build:** empty java options breaks build (7752e8404f13bca547db2b7d29caa6961d970fb0)

### Other changes

- Merge branch 'fix-empty-java-options-bug' into 'master' (f88350b209bc8ced9ecbfb45ec6eb3d64fc980f5)

## 0.7.13
2021-06-23

### Fixes

- **assembler:** fix bug from last release (d66b3d0b1f6f13c621055c0a33401d1dfaa88eeb)
- **ci:** vscode ignore was broken in vsce 1.94 (03b87e9d6b6c46d9c2deac9e8b6cc91e499211c4)

### Other changes

- update release notes (67f653273b776ed45295a399e3cf017fdfc2b6e8)
- Merge remote-tracking branch 'origin/master' (c38dba6db67372e73be4d750ba15a3c79ea4d6a9)
- Merge branch 'fix-java-runtime-error' into 'master' (fe62db4619d8218cfb0b9f066667ba603bd21270)
- Merge remote-tracking branch 'origin/master' (c325594f138152f7d1cef78b14080b51bcdc2e20)

## 0.7.12
2021-06-22

### Fixes

- **ci:** remove hard coded version fix (17540665b05288f120578de0547832fdbf3719a7)
- **ci:** missing next release call (45723333cd2bb6122c087d3b067c0f7eb7a5792d)

## 0.7.10
2021-06-22

### Fixes

- **ci:** need to allow same version again (dbb55989e48a8f815e3d57e9b9d50bb83c211db6)

## 0.7.9
2021-06-22

### Fixes

- **ci:** correcting automated versioning (00925646d3f392685adcf937515d0b8ceb686987)

### Other changes

- Update .gitlab-ci.yml (944fcd46a0aaa3ea7f01156b36ca9171546b6e98)

## 0.7.8
2021-06-22

### Fixes

- replaced deprecated assembler settings (965690a01c2aa85478001a87a68d0aefc4fae5ea)
- add new java settings (de4dfae406697c039763adfe31b233837417652f)
- **settings:** use temp file for version checking (c1da8039d1c48e9abb19c261910eb8e86ddab501)
- **version:** bump to correct version (7c6dfbce4793d5403164620838add47beabc01e0)
- **version:** trying to bump version again (3149812bdb832398d4a2e7fe9c3fd57477bf09b3)
- **version:** modify job to bump if no changes present (bb82c2bb8545316e95dabc1a399337edc1d9305a)
- **ci:** remove allowing same version (9195b335527a352a45471c800aa89433380204a4)

### Other changes

- Merge branch 'feature-java-command-options' into 'master' (b324244dec8518cc279f0612441a5129d0674b52)
- Merge remote-tracking branch 'origin/master' (c09ccf906ea7444a3c307a044ce77c0692d3f8b3)
- bump to 0.7.8 (d701245313d4fdbfc3f2510a18a12cc5dbac6530)
- Update package.json (35a6581f6fb1e60462259ad0bc6659d2e9f42a71)
- Update .gitlab-ci.yml (8f9604b05cfb98f43f2f714228a2418413e89056)
- Update .gitlab-ci.yml (91286004f039c6a7aa056da435f5c0c6dd51e50e)
- Update .gitlab-ci.yml (9c808cb9ceba60f1c4cb3ff8c87a7f08244af6b8)
- Update .gitlab-ci.yml (7a4abd68a6d4fac9870f678a4f1052634fdcd7b6)
- Update .gitlab-ci.yml (ec0e53c68a95f27613b5df1ed4880ccf5471354f)
- Update .gitlab-ci.yml (8ef47473c11e6336bfeadc1a79e01c6f3abde19b)

## 0.7.8
2021-06-22

### Fixes
- correcting version number

## 0.7.7
2021-05-05

### Fixes

- **build:** client build breaking when no errors (2bf4c818c1c8d94ec1f71a9e61a555d034c6de82)

### Other changes

- Merge branch 'fix-run-emulators' into 'master' (4ee13352bcd30cf0d2f4559b9be90678e11764de)

## 0.7.6
2021-05-03

### Fixes

- **assembler:** add 5.20 support (edd395c71a2ddfc5c39bb571113b0e838d0cc88c)

### Other changes

- KickAssembler 5.20 support and some fixes (814e75a3ec6856e95f7176d5cb67d480c110ca33)
- Merge branch 'kickAss520Support' into 'master' (379114c4b19e1adf2ef98e9a1b985a7834550034)

## 0.7.5
2021-05-02

### Fixes

- **hover:** catch non existing assemblerinfo (77ccd3611efaf8330686f69094bcd6af362d0294)

### Other changes

- Merge branch 'fix/98/ignoreMissingPrerequesites' into 'master' (9b49d95db871382d903c818014884d643d9e6227)

## 0.7.4
2021-04-16

### Fixes

- **ci:** show changelog before publish (eaa51a67759fa8029868d6ce341565c3b49ae37c)

### Other changes

- fix(ci) : add changelog to build (4816c9974a5f2426e30acf1831e4f7e840a500b7)

## 0.7.3
2021-04-14

### Fixes

- **doc:** update release notes (0fa2141997216614d6f5c877089feb08e6d5a4b2)
- **snippet:** misleading snippet placeholders (daf7853b4951f4546c4fad184f4bc7bd1ac1f92c)

## 0.7.2
2021-04-11

### Fixes

- **references:** macro names were not found (e06849f49b0659d4b0748ed0303d33763ee8ddef)
- **definition:** recognize labels in imm mnemonic (3589e045453c01a65869fff3688efa8612ce94d9)
- **snippets:** errorif snippet misses a comma (3ac7c071bc9e7383ea87ec594411c5c5e1a7f730)
- **diagnostic:** support build errors in includes (d8819678daf0ef47c69681da2e47beb52f03674d)

## 0.7.1
2021-03-31

### Fixes

- **config:** better error checking for key settings (c70573f8d6f9cadd5e09f50997180e2b4894b320)

## 0.7.0
2021-03-27

### Features

- **assembler:** Add -binfile Support to Assembler (a18ac6f8edce2dc9cfb9ea4eee1414f4134fa44a)
- **diagnostic:** show build errors in diagnostic provider (4bf788f7a8c7a92117afbdd232d089bb62e4be90)

### Fixes

- added information for commit messages when contributing (6609a27219f9c6aef8fd1d0ae1707a3f6f50a34e)
- **fs:** some checks were still using old exists method (b58153ec9f916e208475b0c236a29c820e6d62a1)
- **ci:** add changelog to version stage (f8eb6ac1781995126ceb535435d1e55644445f36)
- **ci:** do not include other changes in final changelog (82de68d2b6ec33ce8206bf2d51187e13b7b83c7a)

## 0.6.4
2021-03-25

### Fixes

- **ci:** fix release link (ee1b7db5762aa7f67226c05cd8886d87527d733e)

## 0.6.3
2021-03-25

### Fixes

- **build:** changed version number (409eb86c92940370e28c97960ec1c087fb9ad2ef)

## 0.6.2
2021-03-25

### Fixes

- **folding:** support nested scope folding (37506e77c5719520d3c772ac1259c69fbfab37f5)
- **ci:** added ci build (2e61107fe4312ea16d342f82b6075ca7265ffcc7)
- **build:** added package-locks (c9795047c960c67d856dce86acb8e2f2dd633c07)

## v0.6.1
### Changes
- `.printnow` support added
## v0.6.0
### Changes
- [Issue 18](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/18) added new Memory Map View for Assembled Programs
## v0.5.6
### Fixed
- [Issue 86](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/86) libdir path failed to find files
## v0.5.5
### Changes
- support for the Kick Assembler option to produce [Byte Dumps](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/94)
- added support for [Reference Provider](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/92/diffs#4d1b91c5d30469849735c2ef4f4ea8a5e1eaa00b)
- added support for [Mega65](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/93)
### Fixes
- [Issue 57](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/57) scoped symbols not showing anything on hover
## v0.5.3
### Changes
- change made to ensure that `!` was triggering autocompletion
### Fixes
-[Issue 79](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/79) - Missing hover description on first line
- [Issue 80](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/80) - Immediate decimal value gets autocompleted with var, const, labels, etc.
- [Issue 83](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/83) - When renaming a var, const etc. by adding a number, autocompletion picks up vars, consts, labels etc
## v0.5.2
### Changes
- all keyboard shortcuts will now only activate when editing a kickassembler file or selecting a file from the Explorer (matched by extension) 
### Fixes
- [Issue 84](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/84) non kickassembler source files were being deleted when being compiled and there was no output folder specified on settings
## v0.5.1
### Changes
# added a few more missing directives, symbols from the tmLanguage file
# some code cleanup

## v0.5.0
### Changes
+ support for 5.17 which removes need for `.asminfo.txt` file (now uses stdout)
### Fixes
+ [Issue 77](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/77)number of missing directives in the tmLanguage file resulted in some not highlighting
+ [Issue 74](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/74)better support for macOS when choosing to run `binary` or `.app` for emulator runtime
## v0.4.14
### Changes
+ update vscode libraries to 3.15 protocol
### Fixed
+ `#if` was not highlighting properly when used with `!` and `@`
## v0.4.13
### Changes
+ small update for .print handling
+ added new setting for using a different Main class for different kickass jar files
## v0.4.12
### Fixed
+ [Issue 60](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/60) - strange issue with .print and .break
## v0.4.11
### Fixed
+ corrected error for autostarting on windows using the F6 key -- mistakenly removed "-autostart" option :(
## v0.4.10
### Fixed
+ [Issue 71](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/71) - intellisense continually pops up without typing character first
+ [Issue 69](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/69) - literal values not showing up on parameters for pseudocommands
## v0.4.9
### Changes
+ [removed keepWorkFiles setting ](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/67)
### Fixed
+ [Issue 66](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/66)

## v0.4.8
### Fixed
[Issue 63](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/63)

## v0.4.7
### Changes
[kick 5.16 version check](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/74)
[support for ascii encoding](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/75)
### Fixed
[Issue 38](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/38)

## v0.4.6
### Changes
+ [Collect symbol namespaces from .namespace if they have the same name to avoid multiple same symbols and suggestions](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests?scope=all&utf8=%E2%9C%93&state=all)
### Fixed
+ [Issue 58](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/58) - errors on imported files for new KickAss 5.13

## v0.4.5
### Changes
+ [updated kick version check to 5.13](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/65)
+ [limited support for pseudocommands (autocomplete only)](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/64)
+ [support for multiple definitions](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/67)
### Fixed
+ [fixed keepWorkFiles setting](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/63)
+ [fixed `.print` support ghosting](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues/59)

## v0.4.4
### Changes
+ [support for parenthesis on `.print` directive](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/62)
### Fixed
+ [Issue 53](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/53) - errors when saving (removed keepWorkFiles setting)

## v0.4.3
### Changes
+ [Automatically create/delete/adjust breakpoints when inserted manually](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/60)
### Fixed
+ [Issue 61](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/61) bug fix on manually inserted `.print`
+ fix errors when startup setting was not located in the workspace folder

## v0.4.2
### Changes
+ [allow addition of manual adding or changing break/print](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/60)
### Fixed
+ [issue 51](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/51) - opening empty file with no startup causing errors
+ [issue 52](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/52) - fix errors with new/empty files

## v0.4.1
### Changed
+ [multilabel support](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/58)
+ added delay setting for onChange trigger setting
+ added message to let developer know that the extension is ready (activated) and what version is running
+ removed the initial build message to make it a little less chatty
+ [support for external breakpoints](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/57)
### Fixed
+ [vscode slow and producing many errors](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/43)
+ [fixed build errors when vscode first starting and no file opened](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/47)
+ [CompletionProvider scope error occuring when new lines added to open file](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/46)
+ [confirm active file or startup before building](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/44)
+ [ignore whitespace in pure dividers as well](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/56)
+ [multiline comments broke symbol display](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/55)
### Known Issues
+ [imported files in a STARTUP project will not properly assemble changes until it is saved](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/48)

## v0.4.0
### Changed
+ [added startup control file and changed default key bindings](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/35)
+ [added some missing syntax highlighting](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/47)
+ [support directive on same line after labels](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/48)
+ [support namespaces and scope](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/50)
### Fixed
+ [hover bug fixes for labels](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/49)
+ [Issue 41](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/41) fix missing directives on hover
+ [Issue 40](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/40) out of scope macros should not be available in completion provider

## v0.3.3
### Changed
+ [added definition provider for goto support](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/44)
+ [added more support for -libdir command](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/37)
+ [added goto support for imports](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/merge_requests/46)
### Fixed
+ [Issue 33](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/33) switching tabs on opened files breaks everything
+ [Issue 30](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/30) space in paths on emulator and debugger could cause problems on mac/linux

## v0.3.2
### Fixed
+ small bug in the version checker that was added in 0.3.1

## v0.3.1
### Changed
+ debugger uses `-symbols` not `-vicesymbols`
+ named labels `label:` where being suggested for preprocessors
+ kick assembler version checking
+ support additional library paths
### Fixed
+ optional vice symbols
+ fixed emulator option not being used
+ removed `-a` parameter for emulator
+ fix backslashes in strings breaking completion
+ prevent word suggestion on empty file lists

## v0.3.0
### Changed
+ added support for 65c02, DTV and Illegal opcodes
+ optional parameter completion hints
+ better output setting support for relative and fixed paths
+ [Issue 27](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/27) completion for filenames when using `.import` or `#import`
+ [Issue 17](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/17) support for .eval var

### Fixed
+ [Issue 26](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/26) code completion for macros and functions was broken
+ [Issue 29](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/29) supress intellisense and code completion inside remarks/comments
+ [Issue 32](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/32) building when focused on output window produced errors
+ [Issue 34](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/34) empty output file setting would cause issues

## v0.2.7
### Changed
+ added new version info to the asminfo file for future use
+ better overall formatting for hovers
### Fixed
+ issue #24 -- symbols with same name only showing the first type registered (hover refactoring)
+ fix preprocessor completion items not showing because of name mismatch

## v0.2.6
### Changed
+ some small changes to the code snippets for whitepace and value completion for .var and .label directives
+ changes to the README to help developers interested in the extension to see all of the new features that have been added
### Fixed
+ fix code snippets to include completion items -- not that this also requires that the editor setting "editor.suggest.snippetsPreventQuickSuggestions" is set to FALSE

## v0.2.5
### Changed
+ code snippets and new directive support (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/27) - Lubber
+ snippets for functions and macros (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/28) - Lubber
+ change icons for labels to match in the outline view
### Fixed
+ named labels hover fixed (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/29) - Lubber

## v0.2.4
### Fixed
+ fixed issue #22 where files could not be read on macos and linux (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/22)

## v0.2.3
### Fixed
+ remove extra hover lines (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/26) - Lubber

## v0.2.2
### Changed
+ detect inline comment as remark and handle original value (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/23) - Lubber
+ support code folding (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/25) - Lubber
+ add missing built-in functions (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/24) - Lubber
### Fixed
+ fix path issue on mac/linux on builds (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/2)

## v0.2.1
### Fixed
+ missing semver dependency on new language server libraries

## v0.2.0
### Changed
+ add new default options C64Debugger (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/3) - Lubber
+ new output directory option when compiling
+ added support for .var directive
+ hover cleaned up for some symbols like .const, .var and .label
+ show correct directive for label symbols (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/6) - Lubber
+ recognize high/low literals (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/7) - Lubber
+ add support for single line comments and and correct remark overlapping (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/9) - Lubber
+ add option to determine when to assemble (onChange or onSave) (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/12) - Lubber
+ add badge info to readme (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/10) - Lubber
+ support for -debugdump on compile (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/13) - Lubber
+ added better support for built-in Constants, Macros and Functions (Hover and Completion)
+ support for pre-processor boolean variables - Lubber
+ support for high/low triggers - Lubber
+ dont provide intructions, but only symbols/macros/etc when a directive was the previous token - Lubber
+ recognize and deny completion inside line comments or string definitions - Lubber
+ fix function/macro parameter hover (last param missed comma separation) - Lubber
+ show docs, examples and comments for completion items (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/19)- Lubber
+ correctly fetch base symbol with methods added (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/22) - Lubber
+ hovers show which file symbols were imported from
### Fixed
+ thanks to Lubber for some general cleanup of code
+ make sure that symbols are updated on outline and in completion (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/issues/13) - Lubber
+ fix undefined errors on hover (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/5) - Lubber
+ fix comments on functions (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/8) - Lubber
+ removed lorem ipsum from opcode hover (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/11) - Lubber
+ upgraded language server (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/20) - Lubber
+ hover over zero values was broken (https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/merge_requests/21) - Lubber

## v0.1.15
### Changed
+ mistaken package update with version change (no actual changes)

## v0.1.14
### Changed
* added hover for macros and functions with comments
* some coverage for parameters on hover
* better symbol coverage
### Fixed
* command pallette entries missing (thanks Wanja Gayk for finding this)

## v0.1.13
### Changed
* added command to run last build (ctrl+f5)
* added command to debug last build (ctrl+f6)
### Fixed
* validate settings on server
* fixed output stealing focus on compile
* better settings validation
* better remark searching
